<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>PASIM | Rescuer</title>

  <?php $this->load->view('_source/landing_top.php'); ?>

</head>

<body class="bg-dark">

<div class="body-inner">

	<!-- Content Start-->
<section style="top: 60px">
	<form method="post" action="<?=base_url('Rescuer/rescue_password') ?>">
	<div class="container text-center">
		<div class="text-center">
			<h3 style="color: white"><i class="fa fa-user"></i><strong> <?=$username ?></strong></h3>
			<h4 style="color: white">Set New Password</h4>
			<p>Enter your new Password twice</p>
			<hr>
			<p style="color: red;"><?= $this->session->flashdata('notify'); ?></p>
			<p style="color: lime;"><?= $this->session->flashdata('success'); ?></p>
		<div class="container text-center col-md-4" style="top: 25px">
			<div class="form-group">
				<input placeholder="New Password" type="password" name="newpassword" class="form-control form-control-lg">
			</div>

			<div class="input-group form-group">
				<input placeholder="Re-type new password" type="password" name="repassword" class="form-control form-control-lg">
			</div>
			<input type="hidden" name="number" value="<?=$id_user ?>">
			<input type="hidden" name="random" value="<?=$token.$temp ?>">
			<div class="form-group">
				<button class="btn btn-warning col-md-12" type="submit"> Confirm</button>
			</div>

			<div class="form-group">
				<a style="color: white; text-decoration: underline;" href="<?=base_url('login') ?>" >Go to Login Page</a>
			</div>


		</div>

	</div>

</form>
</section>


	<!-- Content End -->

</div><!-- End Inner Body -->
  <?php $this->load->view('_source/landing_bottom.php'); ?>

</body>
</html>