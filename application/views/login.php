<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>PASIM | Login</title>

  <?php include '_source/landing_top.php'; ?>

</head>

<body class="bg-dark">

<div class="body-inner">
	<!-- Content Start-->
<section>
	<form method="post" action="<?=base_url('Auth/authentication') ?>">
	<div class="container text-center">
		<div class="text-center">
			<img style="max-width: 100px;" class="logo-center" src="<?= base_url('assets/landing/images/favicon.png')?>">
			<h3 style="color: white">Login Page</h3>
			<p style="color: red;"><?= $this->session->flashdata('notify'); ?></p>
			<hr>
		<div class="container text-center col-md-4" style="top: 25px">
			<div class="form-group">
				<input placeholder="Username" type="text" name="username" class="form-control form-control-lg">
			</div>

			<div class="input-group form-group">
				<input placeholder="Password" id="password" type="password" name="password" class="form-control form-control-lg">
				<span class="input-group-addon" onclick="change()"  id="see">
                 <p class="form-control"><i class="fa fa-eye"></i></p>
                </span>
			</div>

			<div class="form-group">
				<button class="btn btn-info col-md-12" type="submit"> Login</button>

			</div>

			<div class="form-group" >
				<a style="color: white" href="<?=base_url('Main/account_help') ?>">Need Login Help?</a ><br>
			</div>

		</div>

		</div>

	</div>

</form>
</section>

<div class="fomr-group text-center">
	<a style="color: #17a2b8;" href="/advisors">Back to Home</a>
	</div>

	<!-- Content End -->

	<!-- Copyright start -->
	<section id="" class="">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12 text-center">
	        <ul class="footer-social unstyled">
	          <li>
	            <a style="color: white;" href="">Terms </a>-
	            <a style="color: white;" href="">Help </a>-
	            <a style="color: white;" href="">Privacy </a>-
	            <a style="color: white;" href="">Author </a>
	          </li>
	        </ul>
	      </div>
	    </div>
	    <!--/ Row end -->
	    
	    <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
	      <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
	    </div>
	  </div>
	  <!--/ Container end -->
	</section>
	<!--/ Copyright end -->

</div><!-- End Inner Body -->
<?php include '_source/landing_bottom.php'; ?>

</body>

<script type="text/javascript">
            function change()
         {
            var x = document.getElementById('password').type;

            if (x == 'password')
            {
               document.getElementById('password').type = 'text';
               document.getElementById('see').innerHTML = '<span class=""input-group-addon"> <p class="btn form-control"><i style="color:#0191dd" class="fa fa-eye"></i> </p> </span> ';
            }
            else
            {
               document.getElementById('password').type = 'password';
               document.getElementById('see').innerHTML = '<span class=""input-group-addon"> <p class="btn form-control""><i class="fa fa-eye"></i> </p> </span> ';
            }
         }
        </script>


</html>