<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>PASIM | Invalid</title>

  <?php include '_source/landing_top.php'; ?>

</head>

<body class="bg-white">

<div class="body-inner">

	<!-- Content Start-->
<section style="top: 90px">
	<form method="post" action="<?=base_url('Auth/authentication') ?>">
	<div class="container text-center">
		<div class="text-center">
				<h3>This is not Valid Link</h3>
			<hr>
		<div class="container text-center col-md-12">

			<div class="text-center form-group">
				<p>The link you followed may be broken, Expired, or the page may have been removed. <a class=""href="/advisors">Go Back to Home</a> | <a class=""href="/advisors">Go Back to Home</a></p>
			</div>

			<div class="form-group text-center">
				
			</div>
		</div>

		</div>

	</div>

</form>
</section>


	<!-- Content End -->
	<!-- Copyright start -->
	<section id="" class="">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12 text-center">
	        <ul class="footer-social unstyled">
	          <li>
	            <a href="">Terms </a>-
	            <a href="">Help </a>-
	            <a  href="">Privacy </a>-
	            <a href="/advisors">Home </a>
	          </li>
	        </ul>
	      </div>
	    </div>
	    <!--/ Row end -->
	    
	  </div>
	  <!--/ Container end -->
	</section>
	<!--/ Copyright end -->

</div><!-- End Inner Body -->
<?php include '_source/landing_bottom.php'; ?>

</body>
</html>