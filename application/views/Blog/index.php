<!DOCTYPE html>
<html lang="">

<head>
  <meta charset="utf-8">
  <title>PASIM +</title>

  <?php $this->load->view('_source/landing_top.php'); ?>

</head>

<body>

	<div class="body-inner">

	<?php $this->load->view('_partials/blog_navbar'); ?>

<div id="banner-area">
	<img src="<?=base_url('assets/landing/images/banner/cover-blog.jpg')?>" alt="" />
	<div class="parallax-overlay"></div>
	<!-- Subpage title start -->
	<div class="banner-title-content">
		<div class="text-center">
			<h2>Blog Site</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-center">
					<li class="breadcrumb-item text-white" aria-current="page">News - Event - Fun - Tech - Tips & Tricks</li>
				</ol>
			</nav>
		</div>
	</div><!-- Subpage title end -->
</div><!-- Banner area end -->
	<!-- Main container start -->
<section id="main-container">
	<div class="container">
		<!-- Services -->
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon classic float-left"><i class="fa fa-newspaper-o"></i></span>
				<h3 class="title classic">Let's Check this out</h3>
				<hr/>
			</div>
		</div>

		<div class="row">
			<?php foreach ($getAllBlog as $gab) {?>
			<div class="border col-md-4 col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<div class="service-content">
					<a class="" href="<?=base_url('blog/article/'.$gab->blog_url) ?>"><span class="service-image"><img class="img-fluid" src="<?=base_url('images/_blog/'.$gab->blog_img)?>" alt="cover" /></span></a>

					<h3><a style="color: black" href=""><?=$gab->blog_title ?></a></h3>
					<p><i class="fa fa-pie-chart"></i> <?=$gab->blog_type ?><br><i class="fa fa-calendar-minus-o"></i><?=$gab->blog_date ?></p>
				</div>
			</div>
			<!--/ End first service -->

		<?php } ?>

		</div><!-- Content Row end -->
		<!-- Services end -->
	</div>

	

<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img width="20%" src="<?php echo base_url('assets/img/tag-2.png')?>" alt="logo">
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; <?=date('Y') ?> <span>System And Structure Designed  by <a href="https://instagram.com/raihan_cf" target="_blank">Raihan Akbar</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
<?php 
/*
<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img src="<?php echo base_url('assets/landing/images/logo.png')?>" alt="logo">
				</div>
				<div class="gap-20"></div>
				<ul class="dark unstyled">
					<li>
						<a title="Twitter" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
						</a>
						<a title="Facebook" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
						</a>
						<a title="Google+" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
						</a>
						<a title="linkedin" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
						</a>
						<a title="Pinterest" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
						</a>
						<a title="Skype" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
						</a>
						<a title="Dribble" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; Copyright 2019 Themefisher. <span>Designed by <a
							href="https://themefisher.com">Themefisher.com</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
*/
 ?>
</div><!-- Body inner end -->

<?php $this->load->view('_source/landing_bottom.php'); ?>
<?=$this->session->flashdata('notify'); ?>

</body>

</html>