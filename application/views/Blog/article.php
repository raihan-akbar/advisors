<!DOCTYPE html>
<html lang="">

<head>
  <meta charset="utf-8">
  <title>PASIM +</title>

  <?php $this->load->view('_source/landing_top.php'); ?>

  <?php foreach ($getTouchBlog as $gtb) {} ?>

</head>

<body>

	<div class="body-inner">

	<?php $this->load->view('_partials/blog_navbar'); ?>

<div id="banner-area">
	<img src="<?=base_url('assets/landing/images/banner/cover-blog.jpg')?>" alt="" />
	<div class="parallax-overlay"></div>
	<!-- Subpage title start -->
	<div class="banner-title-content">
		<div class="text-center">
			<h2><?=$gtb->blog_type ?></h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-center">
					<li class="breadcrumb-item text-white" aria-current="page">BLOG SITE</li>
				</ol>
			</nav>
		</div>
	</div><!-- Subpage title end -->
</div><!-- Banner area end -->

	<!-- Main container start -->
<!-- Blog details page start -->
<section id="main-container">
	<div class="container">
		<div class="row">
			<!-- Blog start -->
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<!-- Blog post start -->
				<div class="post-content">
					<!-- post image start -->
					<div class="post-image-wrapper">
						<img style="max-height: 1600px;" src="<?=base_url('images/_blog/'.$gtb->blog_img)?>" class="img-fluid" alt="" />
						<hr><hr>
					</div><!-- post image end -->
					<div class="post-header clearfix">
					<span class="blog-date">Posted : <?=$gtb->blog_date ?></span>
						<h1 class="post-title">
							<?=$gtb->blog_title ?>
						</h1>
						<div class="post-meta">
							<span class="post-meta-author">Posted by  <strong><?=$gtb->blog_author ?></strong></span>
							<span class="post-meta-cats"><i class="fa fa-pie-chart"></i> <strong><?=$gtb->blog_type ?></strong></span>

						</div><!-- post meta end -->
					</div><!-- post heading end -->
					<div class="entry-content">
						<?=$gtb->blog_content ?>
					</div>
					<!-- Author info start -->
					<div class="about-author">
						<div class="author-img float-left">
							<img src="images/blog/author.jpg" alt="" />
						</div>
					</div>
					<!-- Author info end -->

					<div class="gap-30"></div>

					<!-- Post comment start -->
					<div class="comments-form">
						<h3>Leave a comment</h3>
						<form role="form">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Your Name</label>
										<input class="form-control" name="name" id="name" placeholder="" type="text" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Your Email</label>
										<input class="form-control" name="email" id="email" placeholder="" type="email" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Message</label>
								<textarea class="form-control required-field" id="message" placeholder="" rows="4" required></textarea>
							</div>
							<div><br>
								<button class="btn btn-primary" type="submit">Post Comment</button>
							</div>
						</form>
					</div><!-- Comments form end -->
					<hr>


					<div id="post-comments">
						<h3>Coments</h3>
						<hr>
						<ul class="comments-list">
							<li>
								<div class="comment">
									<img class="comment-avatar float-left" alt="" src="images/blog/avator1.png">
									<div class="comment-body">
										<h4 class="comment-author">Jack Ruler</h4>
										<div class="comment-date">March 29, 2014 at 1:38 pm</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In magna ligula, faucibus sed ligula ut,
											venenatis mattis diam. Proin feugiat mollis nibh.</p>
									</div>
								</div>
							</li><!-- Comments-list li end -->
						</ul><!-- Comments-list ul end -->
					</div><!-- Post comment end -->

					
				</div><!-- Blog post end -->
			</div>
			<!--/ Content col end -->

			<!-- sidebar start -->
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

				<div class="sidebar sidebar-right">

					<!-- Blog tags start -->
					<div class="widget">
						<hr>
						<h3 class="widget-title">Details</h3>
						<p>Author : <?=$gtb->blog_author ?></p>
						<p>Posted : <?=$gtb->blog_date ?></p>
						<p>Category : <?=$gtb->blog_type ?></p>
						<hr/>
					</div><!-- Text widget end -->

				</div><!-- sidebar end -->
			</div>
		</div>
		<!--/ row end -->
	</div>
	<!--/ container end -->
</section><!-- Blog details page end -->
<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img width="20%" src="<?php echo base_url('assets/img/tag-2.png')?>" alt="logo">
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; <?=date('Y') ?> <span>System And Structure Designed  by <a href="https://instagram.com/raihan_cf" target="_blank">Raihan Akbar</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
<?php 
/*
<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img src="<?php echo base_url('assets/landing/images/logo.png')?>" alt="logo">
				</div>
				<div class="gap-20"></div>
				<ul class="dark unstyled">
					<li>
						<a title="Twitter" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
						</a>
						<a title="Facebook" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
						</a>
						<a title="Google+" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
						</a>
						<a title="linkedin" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
						</a>
						<a title="Pinterest" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
						</a>
						<a title="Skype" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
						</a>
						<a title="Dribble" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; Copyright 2019 Themefisher. <span>Designed by <a
							href="https://themefisher.com">Themefisher.com</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
*/
 ?>
</div><!-- Body inner end -->

<?php $this->load->view('_source/landing_bottom.php'); ?>
<?=$this->session->flashdata('notify'); ?>

</body>

</html>