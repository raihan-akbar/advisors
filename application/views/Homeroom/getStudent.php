
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php foreach ($getCatchStudent as $get) {} ?>
        <?php foreach ($detailUser as $user) {} ?>

        <?php 
         if ($get->student_stages == 'Warned 1') {
                $a_disable = 'disabled="disabled" style="background-color: lightgrey;"';
                $b_disable = '';
                $c_disable = '';
            }else if ($get->student_stages == 'Warned 2') {
                $a_disable = 'disabled="disabled" style="background-color: lightgrey;"';
                $b_disable = 'disabled="disabled" style="background-color: lightgrey;"';
                $c_disable = '';
            }
            else if ($get->student_stages == 'Warned 3') {
                $a_disable = 'disabled="disabled" style="background-color: lightgrey;"';
                $b_disable = 'disabled="disabled" style="background-color: lightgrey;"';
                $c_disable = 'disabled="disabled" style="background-color: lightgrey;"';
            }
            else{
                $a_disable = '';
                $b_disable = '';
                $c_disable = '';
            }
         ?>

        <?php 
        $name_source = explode(' ', $get->name);
        $array = array($name_source[0]); 
        $swalname = implode(' ', $array);
         ?>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"><?=$get->name ?> </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->

                <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumbnail">
                                                        <img width="200" src="<?= base_url('images/_student/'.$get->student_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                    </div>



                                                    <hr/>

                                                    <div class="text-left">
                                                        <a class="" href="#update-modal" data-animation="silde"  data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">Change Student Information</a><hr/>
                                                        <a class="" href="#avatar-modal" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">Change Avatar</a><hr/>
                                                        <!-- <a class=""  onclick="stat('<?=$get->status  ?>')" style="color: orange;"><b>Change Status</b> </a><hr/> -->
                                                        

                                                        
                                                    </div>

                                                </div>

                                            </div> <!-- end card-box -->

                                        </div> <!-- end col -->

                                        <div class="col-md-8 col-lg-9">

                                            <div class="row">
                                                <div class="col-md-12 col-sm-6">
                                                    <div class="text-right">
                                                    
                                                    </div>

                                                    <h5>Student Details</h5>
                                                    <hr/>

                                                        <h3 style="line-height: 1.5; letter-spacing: 2px">NIS : <?php echo $get->nis; ?></h3>

                                                        <p class="text font-13"><strong>Name :</strong>
                                                        <span class="m-l-15"><?php echo $get->name; ?></span></p>

                                                        <p class="text font-13"><strong>Expertise :</strong>
                                                        <span class="m-l-15"><?php echo $get->name_expertise; ?></span><span> (<?php echo $get->alias_expertise; ?>)</span></p>

                                                         <p class="text font-13"><strong>Grade :</strong>
                                                        <span class="m-l-15"><?php echo $get->grade; ?></span></p>

                                                        <p class="text font-13"><strong>Start Periode :</strong>
                                                        <span class="m-l-15"><?php echo $get->start_period; ?></span></p>

                                                        <p class="text font-13"><strong>Gender :</strong>
                                                        <span class="m-l-15"><?php echo $get->gender; ?></span></p>
                                                        
                                                        <p class="text font-13"><strong>Status :</strong>
                                                        <span class="m-l-15"><?php echo $get->status; ?></span></p>

                                                        <p class="text font-13"><strong>Address :</strong>
                                                        <span class="m-l-15"><?php echo $get->address; ?></span></p>

                                                        <p class="text font-13"><strong>Point :</strong>
                                                        <span class="m-l-15"><?php echo $get->point; ?></span></p>

                                                        <p class="text font-13"><strong>Student Stages :</strong>
                                                        <span class="m-l-15"><?php echo $get->student_stages ?></span></p>

                                                        <p class="text font-13"><strong>Deafault Password :</strong>
                                                        <span class="m-l-15"><?php echo $get->token ?></span></p>

                                                        <hr/>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="card-box widget-box-one widget-one-success">
                                    <div class="wigdet-one-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Current Point</p>
                                        <h2 class="text-dark"><span data-plugin="counterup"><?=$get->point ?> </span> </h2>
                                        <p class="text-muted m-0">Total Point Counter</p>
                                    </div>
                                </div>
                            </div><!-- end col -->



                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="card-box widget-box-one">
                                    <div class="wigdet-one-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Case</p>
                                        <h2 class="text-dark"><span data-plugin="counterup"><?=$countRecord ?> </span> </h2>
                                        <p class="text-muted m-0">Case Record Counter</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="card-box widget-box-one">
                                    <div class="wigdet-one-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Achievement</p>
                                        <h2 class="text-dark"><span data-plugin="counterup"><?=$countAchievement ?> </span> </h2>
                                        <p class="text-muted m-0">Achievement Record Counter</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>Warned Letter List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        This is a <?=$get->name ?> Warning Letter List. 
                                    </p>

                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Warning Type</th>
                                            <th>Letter Date</th>
                                            <th>Concerning Subject</th>
                                            <th>Author</th>
                                            <th>More</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($getCSWarningLetter as $cswl) {?>
                                            <tr>
                                                <td><?=$cswl->warned_type  ?></td>
                                                <td><?=$cswl->date_show  ?></td>
                                                <td><?=$cswl->concerning_subject  ?></td>
                                                <td><?=$cswl->s_name ?></td>
                                                 <td><a href="<?php echo base_url().'Homeroom/catchLetter/'.$cswl->id_warnings;?>">Overview</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        </div>
                        <!-- End row -->
        
        <!-- END CONTENT -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


           

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

 <!-- Modal Update -->
<form  action="<?php echo base_url(). 'Master/updStudent' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
    <div id="update-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Update Details</h4>
                                                    <input type="hidden" name="id_student" value="<?=$get->id_student ?>">
                                                </div>
                                                <div class="modal-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">NIS</label>
                                                                <input type="text" id="field-3" name="nis" class="form-control" placeholder="NIS" required value="<?=$get->nis ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name" class="form-control" placeholder="Full Name" required value="<?=$get->name ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Expertise</label>
                                                                <select name="id_expertise" id="field-2" class="form-control" required >
                                                <!-- <option disabled="true" selected="selected">Expertise</option> -->
                                                <option selected="selected" style="background-color: grey" value="<?=$get->id_expertise ?>"><?=$get->alias_expertise ?></option>
                                                <?php foreach ($getExpertise as $e) {
                                                 ?>
                                                <option value="<?php echo $e->id_expertise ?>"><?php echo $e->alias_expertise; ?></option>
                                            <?php } ?>
                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Gender <small></small></label> 
                                                               <select name="gender" class="form-control" required>
                                                                   <option selected="selected"  style="background-color: grey" value="<?= $get->gender ?>"><?= $get->gender ?></option>
                                                                   <option value="Male">Male</option>
                                                                   <option value="Female">Female</option>
                                                               </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Address</label>
                                                                <textarea class="form-control autogrow" placeholder="Student Home Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="address" required><?= $get->address ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Year Added <small></small></label>
                                                               <input type="text" id="field-3" name="start_period" class="form-control" placeholder="Point" value="<?= $get->start_period ?>" required>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                                <label for="field-3" class="control-label">Grade</label>
                                                                <select required name="grade" class="form-control">
                                                                    <option style="background-color: grey" value="<?= $get->grade ?>" selected="selected"><?= $get->grade ?></option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                </select>
                                                            </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                    <div class="form-group">
                                                                <label for="field-3" class="control-label">Student Stages</label>
                                                                <select name="student_stages" class="form-control">
                                                                    <option style="background-color: grey; color: white;" value="<?= $get->student_stages ?>" selected="selected"><?= $get->student_stages ?></option>
                                                                    <option style="background-color: green; color: white;" value="Ordinary">Ordinary</option>
                                                                    <option style="background-color: darksalmon; color: white;" value="Supervised">Supervised</option>
                                                                </select>
                                                            </div>
                                                            <p>Read <u><a href="">Advisors Guide</a></u> if You Not Understand.</p>
                                                    </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-teal waves-effect waves-light">Save +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->


<!-- Modal Avatar Update -->
<form id="myForm" action="<?php echo base_url(). 'Master/updAvaStudent' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
    <div id="avatar-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Update Avatar +</h4>
                                                    <input type="hidden" name="id_student" value="<?=$get->id_student ?>">
                                                </div>
                                                <div class="modal-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Current Avatar</label>
                                                                <div class="thumbnail">
                                                                    <img width="200" src="<?= base_url('images/_student/'.$get->student_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">New Avatar</label>
                                                                <div class="">
                                                                    <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary" required data-parsley-fileextension='jpg'>
                                                                     </div><hr>

                                                                     <div class="">
                                                                        <a onclick="del('<?=$get->student_avatar ?>')" data-dismiss="modal" style="color: red">Delete Current Avatar <i class="fa fa-trash"></i></a>
                                                                     </div><hr>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Ava Update -->

        <!-- Modal Add Vio -->
<form action="<?php echo base_url('Master/addRecord'). '#' ?>" method="post" data-parsley-validate enctype="multipart/form-data">
    <div id="add-record" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add Violation Record +</h4>
                                                </div>
                                                <div class="modal-body">
                                                        
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Violation Name</label>
                                                                <div>
                
                                                                <select id="id-vio" name="id_offense" class="form-control selectpicker" data-live-search="true" required>
                                                                    <option disabled="disabled" selected="selected">Select</option>
                                                                    <?php foreach ($getOffense as $of) {
                                                                     ?>
                                                                    <option value="<?php echo $of->id_offense; ?>"><?php echo $of->name_offense; ?></option>
                                                                <?php } ?>
                                                                </select>

                                                                
                                                                </div>

                                                            </div>
                                                        </div>

                                                       <div class="col-md-6">
                                                                <label class="control-label">Case Date</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <input type="text" name="d_record" id="datepicker-autoclose" class="form-control" data-date-format='dd-mm-yy' required  value="<?= date('d-m-Y') ?>">
                                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span> 
                                                                </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">About</label>
                                                                <textarea class="form-control autogrow" placeholder="Explain About this Case / Penalty" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_record" required></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Point Added</label>
                                                                <input type="number" min="0" name="cost" id="cost" class="form-control" placeholder="Cost of Point Added" required  data-parsley-min="0">
                                                            </div>

                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Image Documentation</label>
                                                               <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary">
                                                            </div>
                                                            <hr/>
                                                            <div>
                                                                <p class="strong">Please read the <a href="<?=base_url('Main/terms') ?>" target="_BLANK"><u>Case Recording Terms</u></a> before Adding a Case Record</p>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add Record+</button>
                                                    <input type="hidden" name="id_student" value="<?php echo $get->id_student ?>">
                                                    <input type="hidden" name="id_user" value=" <?php echo $user->id_user ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>

                                <!-- Modal Add Ach -->
<form action="<?php echo base_url('Master/addAchievement'). '#' ?>" method="post" data-parsley-validate enctype="multipart/form-data">
    <div id="add-achievement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add Achievement +</h4>
                                                </div>
                                                <div class="modal-body">
                                                        
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Title</label>
                                                                <div>
                                                                    <input type="text" name="achievement_title" class="form-control" placeholder="Achievement Title" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Achievement Date</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <input type="text" name="date_achievement" id="datepicker" class="form-control" data-date-format='dd-mm-yy' required  value="<?= date('d-m-Y') ?>">
                                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span> 
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">About Achievement</label>
                                                                <textarea class="form-control autogrow" placeholder="Explain About Achievement" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_achievement" required></textarea>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add Record+</button>
                                                    <input type="hidden" name="id_student" value="<?php echo $get->id_student ?>">
                                                    <input type="hidden" name="id_user" value=" <?php echo $user->id_user ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>

<!-- Modal Add Letter -->
<form action="<?php echo base_url('Master/addLetter'). '#' ?>" method="post" data-parsley-validate enctype="multipart/form-data">
    <div id="make-letter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Create Warning Letter</h4>
                                                </div>
                                                <div class="modal-body">
                                                        
                                                    <div class="row">

                                                    <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Title</label>
                                                                <div>
                                                                    <input type="text" name="letter_title" class="form-control" placeholder="Letter Title" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Concerning Subject</label>
                                                                <div>
                                                                    <input type="text" name="concerning_subject" class="form-control" placeholder="Concening Subjects" required>
                                                                </div>
                                                            </div>
                                                     </div>

                                                     <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Warnings Type</label>
                                                                <div>
                                                                    <select class="form-control" name="warned_type" required>
                                                                        <option selected="selected" disabled="disabled">Select Warning Type --</option>
                                                                        <option <?=$a_disable ?> value="Warned 1">Warned 1</option>
                                                                        <option <?=$b_disable ?> value="Warned 2">Warned 2</option>
                                                                        <option <?=$c_disable ?> value="Warned 3">Warned 3</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                     </div>

                                                    </div>

                                                    <div class="row">
                                                    <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Date</label>
                                                                <div>
                                                                    <select class="form-control" name="d_show" required="">
                                                                        <option selected="selected" disabled="disabled">Date</option>
                                                                        <?php for ($ds=1; $ds <=31 ; $ds++) { ?>
                                                                            <option value="<?=$ds ?>"><?=$ds ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Month</label>
                                                                <div>
                                                                    <select class="form-control" name="m_show" required="">
                                                                        <option selected="selected" disabled="disabled">Month</option>
                                                                        <option value="Januari">Januari</option>
                                                                        <option value="Februari">Februari</option>
                                                                        <option value="Maret">Maret</option>
                                                                        <option value="April">April</option>
                                                                        <option value="Mei">Mei</option>
                                                                        <option value="Juni">Juni</option>
                                                                        <option value="Juli">Juli</option>
                                                                        <option value="Agustus">Agustus</option>
                                                                        <option value="September">September</option>
                                                                        <option value="Oktober">Oktober</option>
                                                                        <option value="November">November</option>
                                                                        <option value="Desember">Desember</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Year</label>
                                                                <div>
                                                                    <input type="text" name="y_show" class="form-control" placeholder="Achievement Title" required value="<?=date('Y') ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Warning Reason</label>
                                                                <textarea class="form-control autogrow" placeholder="Explain About the Letter" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="letter_reason" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <p>Please Read <a href=""><u>Letter Guide</u> </a> for Best Experience.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add Letter+</button>
                                                    <input type="hidden" name="id_student" value="<?php echo $get->id_student ?>">
                                                    <input type="hidden" name="id_user" value=" <?php echo $user->id_user ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>

                                
<script>//Swal Delete Offense
 function del(ava) {
        var ava;
        if (ava == 'default.png') {
            swal('Ambigous','Student already use default avatar', 'warning')
            return false;
        }else{
         swal({
        
  title: "Delete Avatar?",
  text: "Are you sure? Your will not be able to recover this avatar! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Delete!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/delAvaStudent/';?>";

});
}
     }
</script>


 <script>//Swal Status
     function stat(status) {
        var status;
        if (status == 'Active') {
        swal({
        
  title: "Deactive Student?",
  text: "Are you sure? This Student is Active if You Deactive this student will be not appear at student list ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Deactive Student",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/delAvaStudent/';?>";

});    
        }else{
         swal({
        
  title: "Delete Avatar?",
  text: "Are you sure? Your will not be able to recover this avatar! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/delAvaStudent/';?>";

});
}
     }
 </script>

    <?= $this->session->flashdata('notify');  ?>

<script>
$(document).ready(function(){
 $('form').parsley();
});
</script>

</html>