
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php foreach ($bundleInfo as $b) {} ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b><?=$this->session->userdata('grade'); ?> - <?=$b->name_expertise ?></b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        This is Your Student List
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#add-modal">Add Homeroom +</a>
                                    </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>NIS</th>
                                            <th>Gender</th>
                                            <th>Point</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php $no = 1; foreach ($notAllStudent as $s) { ?>
                                        <tr>
                                            <td><?=$no ++ ?></td>
                                            <td><?=$s->name ?></td>
                                            <td><?=$s->nis ?></td>
                                            <td><?=$s->gender ?></td>
                                            <td><?=$s->point ?></td>
                                            <td><a href="<?php echo base_url().'Homeroom/catchStudent/'.$s->id_student;?>">More Details </a></td>
                                        </tr>
                                    <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>
</html>