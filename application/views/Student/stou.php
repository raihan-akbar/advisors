
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php foreach ($getCatchTeacherChat as $gctc) {} ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="mails">

                                    <div class="table-box">
                                        <div class="table-detail">
                                            <div class="p-20" style="min-width: 250px;">

                                                <h5 class="hidden-xxs">Teacher List </h5>
                                                <hr/>

                                                <div class="col-md-10">

                                                    <?php foreach ($getAllTeacher as $gat) {?>
                                                <div class="chat-widget hidden-xxs">
                                                    <a href="<?=base_url('Student/catchTeacherChat/'.$gat->id_user) ?>">
                                                        <div class="chat-item">
                                                            <div class="chat-right-text">
                                                                <p class="chat-item-author" style="color: #0077be;"><?=$gat->s_name ?></p>
                                                                <p class="chat-item-text"><?=$gat->username ?></p>

                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                                </div>

                                            </div>
                                        </div>


                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box m-t-20">
                                                        <h4 class="m-t-0"><b><?=$gctc->s_name ?></b></h4>

                                                        <hr/>

                                                        <div class="chat-conversation">

                                            <ul class="conversation-list slimscroll-alt" style="height: 380px; min-height: 332px;">
                                                        
                                                        <li>
                                                        <div class="m-t-2 chat-item">    
                                                            <div class="media-body">
                                                                <p class="pull-left m-b-20 btn-primary btn-rounded">Hey, What'up? </p>
                                                            </div>
                                                        </div>
                                                        </li>

                                            </ul>

                                                    </div>
                                                    <!-- card-box -->
                                                </div> <!-- end col -->
                                                <div class="input-group m-b-20">
                                                    <input class="form-control" value="" placeholder="Type a Message">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn waves-effect waves-light btn-primary">Send <i class="fa fa-paper-plane"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            </div> <!-- end row -->

                                            <!-- End row -->

                                        </div> <!-- table detail -->
                                    </div>
                                    <!-- end table box -->

                                </div> <!-- mails -->
                            </div>
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>


</html>