
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php foreach ($studentBundle as $get) {} ?>
        <?php $con = $this->session->userdata('con'); ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"><?=$get->name; ?> </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
<!-- Start Content Here -->

                <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumbnail">
                                                        <img width="200" src="<?= base_url('images/_student/'.$get->student_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                    </div>

                                                    <div class="text-center">
                                                        <!-- <a class=""  onclick="stat('<?=$get->status  ?>')" style="color: orange;"><b>Change Status</b> </a><hr/> -->
                                                        <hr/>
                                                        <a class="" href="#password-modal" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">Change Password</a>
                                                        <hr/>

                                                        
                                                    </div>

                                                </div>

                                            </div> <!-- end card-box -->

                                        </div> <!-- end col -->

                                        <div class="col-md-8 col-lg-9">

                                            <div class="row">
                                                <div class="col-md-12 col-sm-6">
                                                    <div class="text-right">
                                                    
                                                    </div>

                                                    <h5>Student Details</h5>
                                                    <hr/>

                                                        <h4 style="line-height: 1.5; letter-spacing: 2px">NIS : <?php echo $get->nis; ?></h4>

                                                        <p class="text font-13"><strong>Name :</strong>
                                                        <span class="m-l-15"><?php echo $get->name; ?></span></p>

                                                        <p class="text font-13"><strong>Expertise :</strong>
                                                        <span class="m-l-15"><?php echo $get->name_expertise; ?></span><span> (<?php echo $get->alias_expertise; ?>)</span></p>

                                                         <p class="text font-13"><strong>Grade :</strong>
                                                        <span class="m-l-15"><?php echo $get->grade; ?></span></p>

                                                        <p class="text font-13"><strong>Start Periode :</strong>
                                                        <span class="m-l-15"><?php echo $get->start_period; ?></span></p>

                                                        <p class="text font-13"><strong>Gender :</strong>
                                                        <span class="m-l-15"><?php echo $get->gender; ?></span></p>
                                                        
                                                        <p class="text font-13"><strong>Status :</strong>
                                                        <span class="m-l-15"><?php echo $get->status; ?></span></p>

                                                        <p class="text font-13"><strong>Address :</strong>
                                                        <span class="m-l-15"><?php echo $get->address; ?></span></p>

                                                        <p class="text font-13"><strong>Point :</strong>
                                                        <span class="m-l-15"><?php echo $get->point; ?></span></p>

                                                        <p class="text font-13"><strong>Student Stages :</strong>
                                                        <span class="m-l-15"><?php echo $get->student_stages ?></span></p>

                                                        <hr/>


                                    </div>
                                </div>
                            </div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>

    <!-- Modal Pass -->
    <form action="<?php echo base_url($con). '/updPassword' ?>" method="post" data-parsley-validate novalidate>
    <div id="password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Change Password</h4>
                                                    <input type="hidden" name="id_student" value="<?=$get->id_student ?>">
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">Current Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="old" name="old" class="form-control" placeholder="Your Current Password" required>
                                                                <span class="input-group-addon btn-teal"  onclick="change()"  id="see">
                                                                    Show
                                                                </span>
                                                            </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">New Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="password" name="password" class="form-control" placeholder="Your Old Password" required data-parsley-minlength="8">
                                                                <span class="input-group-addon"  id="v1">
                                                                    <i class="fa fa-lock"></i>
                                                                </span>
                                                            </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">Re-Type New Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="p2" name="p2" class="form-control" placeholder="Your Old Password"
                                                                required data-parsley-equalto="#password">
                                                                <span class="input-group-addon"  id="v2`">
                                                                    <i class="fa fa-keyboard-o"></i>
                                                                </span>
                                                            </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="submit-password" type="submit" class="btn btn-facebook waves-effect waves-light">Save </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                                </form>
<!-- Modal Pass -->
    
<script type="text/javascript">
            function change()
         {
            var x = document.getElementById('old').type;

            if (x == 'password')
            {
               document.getElementById('old').type = 'text';
               document.getElementById('see').innerHTML = '<span style="text-decoration:line-through"> Show </span>';
            }
            else
            {
               document.getElementById('old').type = 'password';
               document.getElementById('see').innerHTML = 'Show';
            }
         }
        </script>

</html>