<!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                   <!--  <a href="index.html" class="logo"><span>Zir<span>cos</span></span><i class="mdi mdi-layers"></i></a> -->

                    <!-- Image logo -->
                    <?php $con = $this->session->userdata('con'); ?>
                    <a href="<?=base_url($con.'/') ?>" class="logo">
                        <span>
                            <img src="<?= base_url('assets/img/tag-2.png') ?>" alt="" height="30">
                        </span>
                        <i>
                            <img src="<?= base_url('assets/img/favicon.png') ?>" alt="" height="28">
                        </i>
                    </a>

                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                            
                            <li>
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
<!-- 
                            <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
 -->
                            <?php date_default_timezone_set("Asia/Jakarta"); ?>
                            <?php $datenow =  date('Y-d-m');?>
                            <?php $dateshow = date('d F, Y') ?>

                            <li class="hidden-xs">
                                <a href="#" class="menu-item"><?=$dateshow;?></a>
                            </li>
<!-- 
                            <li class="dropdown hidden-xs">
                                <a data-toggle="dropdown" class="dropdown-toggle menu-item" href="#" aria-expanded="false">English
                                    <span class="caret"></span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">German</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">Italian</a></li>
                                    <li><a href="#">Spanish</a></li>
                                </ul>
                            </li>
 -->
                        </ul>

                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown user-box">
                                <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                     <i class="fa fa-user"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                        <h5><?=$this->session->userdata('username'); ?></h5>
                                    </li>
                                    <li><a href="<?= base_url($con).'/account/';?>"><i class="ti-user m-r-5"></i> Account</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                                    <li><a href="<?= base_url('Auth/logout') ?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End