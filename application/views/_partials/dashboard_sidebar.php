<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="menu-title">Navigation</li>

                            <?php foreach ($getMenu as $menu) { ?>
                            <li>
                                <a href="<?= base_url($menu->url) ?>" class="waves-effect"><i class="<?= $menu->icon ?>"></i><span> <?= $menu->title ?> </span></a>
                            </li>
                        <?php } ?>

                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                    <div class="help-box">
                        <h5 class="text-muted m-t-0">For Help ?</h5>
                        <p class=""><span class="text-custom">Email:</span> <br/> raihangl19@gmail.com</p>
                        <p class="m-b-0"><a class="text-custom" href="https://t.me/raihan8" target="_blank">Telegram</a></p>
                    </div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->