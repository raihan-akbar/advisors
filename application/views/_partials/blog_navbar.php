<!-- Header start -->
<header id="header" class="fixed-top header2" role="banner">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-dark">
			<a class="navbar-brand" href="<?=base_url('/') ?>"><img class="img-fluid" style="width: 145px"  src="<?=base_url('assets/img/tag-2.png') ?>" alt="logo"></a>
			<button class="navbar-toggler ml-auto border-0 rounded-0 text-black" type="button" data-toggle="collapse"
				data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse text-center" id="navigation">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url('/') ?>">Home</a></a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="<?=base_url('blog/') ?>">Blog</a></a>
					</li>

				</ul>
			</div>
		</nav>
	</div>
</header>
<!--/ Header end -->