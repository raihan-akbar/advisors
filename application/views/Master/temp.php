 <!-- Start Content Here -->
         <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>User List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        Admin can View or Edit User<small>(Advisors/Teacher/Homeroom/Admin)</small> from this page.
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#modal-add">Add User +</a>
                                </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Cost</th>
                                            <th>Date</th>
                                            <th>Advisors</th>
                                            <th>View</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($getRecordList as $i) {?>
                                        <tr>
                                            <td><?=$i->name_offense ?></td>
                                            <td><?=$i->name ?></td>
                                            <td><?=$i->cost ?></td>
                                            <td><?=$i->d_record ?></td>
                                            <td><?=$i->s_name ?></td>
                                            <td><a class="btn btn-teal" data-toggle="modal" data-target="#view-modal<?php echo $i->id_record ?>">View</a></td>
                                        </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

        <!-- END CONTENT -->