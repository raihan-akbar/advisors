
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Case History </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li><i class="fa fa-book"></i></li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->
         <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>User List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        Admin can View or Edit User<small>(Advisors/Teacher/Homeroom/Admin)</small> from this page.
                                    </p>
                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Cost</th>
                                            <th>Date</th>
                                            <th>Advisors</th>
                                            <th><i class="fa fa-eye"></i></th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($getRecordList as $i) {?>
                                        <tr>
                                            <td><?=$i->name_offense ?></td>
                                            <td><?=$i->name ?></td>
                                            <td><?=$i->cost ?></td>
                                            <td><?=$i->d_record ?></td>
                                            <td><?=$i->s_name ?></td>
                                            <td>
                                            <a class="" href="#modal-view<?php echo $i->id_record ?>" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">View</a></td>
                                        </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

        <!-- END CONTENT -->

                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

    <!-- Modal Add Vio -->
    <?php foreach ($getRecordList as $r) { ?>
<form action="<?php echo base_url('Master/addRecord'). '#' ?>" method="post" data-parsley-validate enctype="multipart/form-data">
    <div id="modal-view<?php echo $r->id_record ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">View Case Record +</h4>
                                                </div>
                                                <div class="modal-body">
                                                        
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <div class="thumbnail">
                                                                    <img width="200" src="<?= base_url('images/_documentation/'.$r->doc_img)?>" class="img-rounded img-responsive" alt="image">
                                                                    
                                                                </div>
                                                                <a href="<?= base_url('images/_documentation/'.$r->doc_img)?>" target='_blank'>Click for Full Size</a>
                                                            </div>
                                                        </div>

                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Violation Type</label>
                                                               <p><?=$r->name_offense ?></p>
                                                            </div>
                                                        </div>

                                                       <div class="col-md-6">
                                                                <label class="control-label">Case Date</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->d_record ?></p>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-12"><hr/></div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Student Name</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->name; ?></p>
                                                                </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Student ID</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->nis ?></p>
                                                                </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Expertise</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->name_expertise ?> (<?=$r->alias_expertise ?>)</p>
                                                                </div>
                                                        </div>

                                                        <div class="col-md-12"><hr/></div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Grade</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->grade ?></p>
                                                                </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Advisors</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->s_name ?></p>
                                                                </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Cost Point</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <p><?=$r->cost ?></p>
                                                                </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">About</label>
                                                                <p>
                                                                    <?=$r->about_record ?>
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <hr/>
                                                            <div>
                                                                <p class="strong">Please read the <a href="<?=base_url('Main/terms') ?>" target="_BLANK"><u>Case Recording Terms</u></a> for Best Experience.</p>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
                            <?php } ?>


 <?php echo $this->session->flashdata('notify') ?>

</html>