
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Pending Report </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <p class="text-muted font-13 m-b-30">
                                        You can Accept or Reject Report.
                                    </p>

                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Grade</th>
                                            <th>Expertise</th>
                                            <th>Report By</th>
                                            <th>Offense</th>
                                            <th>Date</th>
                                            <th>More</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($getWaitReport as $get) {?>
                                            <tr>
                                                <td><?=$get->name ?></td>
                                                <td><?=$get->grade ?></td>
                                                <td><?=$get->alias_expertise ?></td>
                                                <td><?=$get->s_name ?></td>
                                                <td><?=$get->d_record ?></td>
                                                <td><?=$get->name_offense ?></td>
                                                 <td><a data-toggle="modal" data-target="#modal-view<?php echo $get->id_report_violation ?>">Overview</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Accepted Report </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <p class="text-muted font-13 m-b-30">
                                        View Accepted Report.
                                    </p>

                                    <table id="datatable-fixed-header"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Grade</th>
                                            <th>Expertise</th>
                                            <th>Report By</th>
                                            <th>Offense</th>
                                            <th>Date</th>
                                            <th>More</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($getAccReport as $get) {?>
                                            <tr>
                                                <td><?=$get->name ?></td>
                                                <td><?=$get->grade ?></td>
                                                <td><?=$get->alias_expertise ?></td>
                                                <td><?=$get->s_name ?></td>
                                                <td><?=$get->d_record ?></td>
                                                <td><?=$get->name_offense ?></td>
                                                 <td><a data-toggle="modal" data-target="#modal-view<?php echo $get->id_report_violation ?>">Overview</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Rejected Report </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <p class="text-muted font-13 m-b-30">
                                        View Accepted Report.
                                    </p>

                                    <table id="datatable-keytable"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Grade</th>
                                            <th>Expertise</th>
                                            <th>Report By</th>
                                            <th>Offense</th>
                                            <th>Date</th>
                                            <th>More</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($getDecReport as $get) {?>
                                            <tr>
                                                <td><?=$get->name ?></td>
                                                <td><?=$get->grade ?></td>
                                                <td><?=$get->alias_expertise ?></td>
                                                <td><?=$get->s_name ?></td>
                                                <td><?=$get->d_record ?></td>
                                                <td><?=$get->name_offense ?></td>
                                                 <td><a data-toggle="modal" data-target="#modal-view<?php echo $get->id_report_violation ?>">Overview</a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>
    <!-- Modal View -->

        <?php foreach ($getAllReport as $o) {  
             if ($o->stat == 'Pending') {
                $txtStyle = 'style="background-color: orange; color: white; text-align: center;"';

                                                   $btnStat = ' <div class="modal-footer">
                                                    <a onclick="dec('.$o->id_report_violation.')" class="btn btn-warning waves-effect">Decline</a>
                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Aggree <i class="fa fa-check"></i></button>
                                                </div>';
                                               }else {$btnStat = '<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>'; $txtStyle = 'style="background-color: gray; color: white; text-align: center;"';}?>

    <form action="<?php echo base_url(). 'Master/accReport' ?>" method="post" data-parsley-validate novalidate>
        <div id="modal-view<?php echo $o->id_report_violation ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: red">×</button>
                                                    <h4 class="modal-title">View Data</h4>
                                                    <input type="hidden" name="id_report" value="<?php echo $o->id_report_violation ?>">
                                                </div>
                                                <div class="modal-body">

                                                    <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <p <?=$txtStyle ?>><?=$o->stat ?></p>
                                                                <div class="thumbnail">

                                                                    <img width="200" src="<?= base_url('images/_documentation/'.$o->doc_img)?>" class="img-rounded img-responsive" alt="image">
                                                                    
                                                                </div>
                                                                <a href="<?= base_url('images/_documentation/'.$o->doc_img)?>" target='_blank'>Click for Full Size</a>
                                                            </div>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Reported By : <?=$o->s_name ?></label>
                                                                <hr/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                     <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Student Name : <span><?=$o->s_name ?></span></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Violation Type : <span><?=$o->name_offense ?></span></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Grade : <span><?=$o->grade ?></span></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Expertise : <span><?=$o->alias_expertise ?></span></label>
                                                            </div>
                                                        </div>

                                                    </div>

                                                     <div class="row">
                                                        <hr/>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Date</label>
                                                                <input readonly="readonly" type="text" class="form-control" placeholder="Cost" name="cost" value="<?=$o->d_record ?>" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Cost</label>
                                                                <input type="text" class="form-control" placeholder="Cost" name="cost" required>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">About</label>
                                                                <textarea readonly="readonly" class="form-control autogrow" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_offense" required><?php echo $o->about_record ?></textarea>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>

                                               <?=$btnStat; ?>

                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>
                                <?php } ?>
</html>

<script>//Swal Delete Offense
     function dec(id) {
  swal({
  title: "Reject Violation Report?",
  text: "Your will not be able to Change this Record Data! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Reject this!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/decReport/';?>"+id;

});

     }
 </script>

