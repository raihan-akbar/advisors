
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->

        <div class="row">
                            <div class="col-sm-12">

                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b>Student List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        <code>Use Search to make it easier to find data</code>
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#add-modal">Add Student +</a>
                                </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>NIS</th>
                                            <th>Expertise</th>
                                            <th>Point</th>
                                            <th>Grade</th>
                                            <th><i class="fa fa-cogs"></i></th>
                                        </tr>
                                        </thead>


                                        <tbody>
            <?php foreach ($getStudent as $get) {  ?>
                                        <tr>
                                            <td><?= $get->name  ?></td>
                                            <td><?= $get->nis  ?></td>
                                            <td><?= $get->alias_expertise ?></td>
                                            <td><?= $get->point ?></td>
                                            <td><?= $get->grade ?></td>
                                            <td><a href="<?php echo base_url().'Master/catchStudent/'.$get->id_student;?>">More Details </a></td>
                                        </tr>
            <?php } ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

        <!-- END CONTENT -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

<!-- Modal Add -->
<form  action="<?php echo base_url(). 'Master/addStudent' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Student +</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">NIS</label>
                                                                <input type="text" id="field-3" name="nis" class="form-control" placeholder="NIS" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name" class="form-control" placeholder="Full Name" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Expertise</label>
                                                                <select name="id_expertise" id="field-2" class="form-control" required>
                                                <option disabled="true" selected="selected">Expertise</option>
                                                <?php foreach ($getExpertise as $e) {
                                                 ?>
                                                <option value="<?php echo $e->id_expertise ?>"><?php echo $e->alias_expertise; ?></option>
                                            <?php } ?>
                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Gender <small></small></label> 
                                                               <select name="gender" class="form-control" required>
                                                                   <option disabled="true" selected="selected">Gender</option>
                                                                   <option value="Male">Male <i class="fa fa-facebook"></i></option>
                                                                   <option value="Female">Female</option>
                                                               </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Address</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Student Home Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="address" required></textarea>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Year Added <small></small></label>
                                                               <input type="text" id="field-3" name="start_period" class="form-control" placeholder="Point" value="<?php echo date('Y') ?>" required>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                                <label for="field-3" class="control-label">Grade</label>
                                                                <select required name="grade" class="form-control">
                                                                    <option selected="selected" disabled="disabled">Select Grade</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                </select>
                                                            </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Student Email</label>
                                                                <input type="email" name="student_email" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Student Avatar</label>
                                                                <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->

<script>
$(document).ready(function(){
 $('form').parsley();
});
</script>

 <?php echo $this->session->flashdata('notify') ?>
</html>