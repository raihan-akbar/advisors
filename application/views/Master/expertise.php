
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->

        <div class="row">
                            <div class="col-sm-12">

                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b>Student List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        <code>Use Search to make it easier to find data</code>
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#add-modal">Add Offense +</a>
                                </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">Alias</th>
                                            <th class="col-md-3">Name</th>
                                            <th>About</th>
                                            <th class="col-md-1"><i class="fa fa-cogs"></i></th>
                                        </tr>
                                        </thead>


                                        <tbody>
            <?php foreach ($getExpertise as $get) {  ?>
                                        <tr>
                                            <td><?= $get->alias_expertise  ?></td>
                                            <td class="col-md-2"><?= $get->name_expertise  ?></td>
                                            <td><?= $get->about_expertise  ?> <?= $get->id_expertise  ?></td>
                                            <td><a class="btn btn-teal" data-toggle="modal" data-target="#modal-view<?php echo $get->id_expertise ?>">
                                                    Details
                                                </a></td>
                                        </tr>
            <?php } ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

        <!-- END CONTENT -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

    <!-- Modal Add -->
<form action="<?php echo base_url(). 'Master/addExpertise' ?>" method="post" data-parsley-validate novalidate>
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Student +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name_expertise" class="form-control" placeholder="Name of this Expertise" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Alias</label>
                                                               <input type="text" id="field-3" name="alias_expertise" class="form-control" placeholder="Alias of this Expertise Name" required> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_expertise" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->

        <!-- Modal View -->

                                            <?php
                                            foreach ($getExpertise as $o) {
                                                
                                             ?>
    <form action="<?php echo base_url(). 'Master/updExpertise' ?>" method="post">
        <div id="modal-view<?php echo $o->id_expertise?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Student +</h4>
                                                    <input type="hidden" name="id_expertise" value="<?php echo $o->id_expertise ?>">
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name_expertise" class="form-control" placeholder="Name of this Expertise" value="<?php echo $o->name_expertise ?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Alias</label>
                                                               <input type="text" id="field-3" name="alias_expertise" class="form-control" placeholder="Alias of this Expertise Name" value="<?php echo $o->alias_expertise ?>" required> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_expertise" required> <?php echo $o->about_expertise ?></textarea>                                                        
                                                            </div>
                                                            <a style="color: red;" onclick="del(<?=$o->id_expertise  ?>)" data-dismiss="modal">Delete Data <i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes <i class="fa fa-pencil"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>

    

<script>//Swal Delete Expertise
     function del(id) {
         swal({
  title: "Delete Expertise Data?",
  text: "This data will affect other data, and Your will not be able to recover this Expertise Data! "+id,
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/delExpertise/';?>"+id;

});

     }
 </script>

 <?php } ?>


 <?php echo $this->session->flashdata('notify') ?>

 <script>
$(document).ready(function(){
 $('form').parsley();
});
</script>

</html>