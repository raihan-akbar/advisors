
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->

        <div class="row">
                            <div class="col-sm-12">

                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b>Student List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        <code>Use Search to make it easier to find data</code>
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#add-modal">Add Offense +</a>
                                    </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th >Level</th>
                                            <th >Offense</th>
                                            <th>About Offense</th>
                                            <th class="col-md-1"><i class="fa fa-cogs"></i></th>
                                        </tr>
                                        </thead>


                                        <tbody>
            <?php foreach ($getOffense as $get) {  ?>

                <?php if ($get->level_offense == 'Low') {
                    $clr = ' style="color: green" ';
                }elseif ($get->level_offense == 'Mid') {
                    $clr = ' style="color: orange" ';
                }
                elseif ($get->level_offense == 'High') {
                    $clr = ' style="color: red" ';
                }

            ?>
                                        <tr>
                                            <td <?=$clr ?>><?= $get->level_offense ?></td>
                                            <td><?= $get->name_offense ?></td>
                                            <td><?= $get->about_offense ?></td>
                                            <td><a class="btn btn-teal" data-toggle="modal" data-target="#modal-view<?php echo $get->id_offense ?>">
                                                    Options
                                                </a></td>
                                        </tr>
            <?php } ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

        <!-- END CONTENT -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

<!-- Modal Add -->
<form action="<?php echo base_url(). 'Master/addOffense' ?>" method="post" data-parsley-validate novalidate>
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Offense +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Name</label>
                                                                <input type="text" class="form-control" id="field-1" placeholder="Name of Offense" name="name_offense" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">

                                                <select name="level_offense" class="form-control" required>
                                                <option disabled="true" selected="selected">Level</option>
                                                <option value="Low" style="background-color: #a6f277">Low</option>
                                                <option value="Mid" style="background-color: #d7de0b">Mid</option>
                                                <option value="High" style="background-color: #c25c3a">High</option>
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Offense" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_offense" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->

    <!-- Modal View -->

        <?php foreach ($getOffense as $o) { ?>
    <form action="<?php echo base_url(). 'Master/updOffense' ?>" method="post" data-parsley-validate novalidate>
        <div id="modal-view<?php echo $o->id_offense ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">View Data</h4>
                                                    <input type="hidden" name="id_offense" value="<?php echo $o->id_offense ?>">
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Name</label>
                                                                <input type="text" class="form-control" id="field-1" placeholder="Name of Offense" name="name_offense" value="<?php echo $o->name_offense ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
<?php 
                    $lvl=$o->level_offense;
                    if ($lvl== "Low") {
                        $low="selected=selected";
                        $mid="";
                        $high="";
                    }
                    elseif ($lvl== "Mid") {
                        $low="";
                        $mid="selected=selected";
                        $high="";
                    }
                    elseif ($lvl== "High") {
                        $low="";
                        $mid="";
                        $high="selected=selected";
}?>
                                                <select name="level_offense" class="form-control" required>
                                                <option disabled="true" selected="selected">Level</option>
                                                <option value="Low" style="background-color: #a6f277" <?php echo $low; ?>>Low</option>
                                                <option value="Mid" style="background-color: #d7de0b" <?php echo $mid ?>>Mid</option>
                                                <option value="High" style="background-color: #c25c3a" <?php echo $high; ?>>High</option>
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_offense" required><?php echo $o->about_offense ?></textarea>
                                                            </div>  
                                                        <a style="color: red;" onclick="del(<?=$o->id_offense  ?>)" data-dismiss="modal">Delete <i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes <i class="fa fa-pencil"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>

    

<script>//Swal Delete Offense
     function del(id) {
  swal({
  title: "Delete Expertise Data?",
  text: "Your will not be able to recover this Expertise Data! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Admin/delUser/';?>"+id;

});

     }
 </script>

 <?php } ?>

 <script>
$(document).ready(function(){
 $('form').parsley();
});
</script>


 <?php echo $this->session->flashdata('notify') ?>


</html>