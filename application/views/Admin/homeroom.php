
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Homeroom </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <i class="fa fa-user-secret"></i>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>Homeroom List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        KeyTable provides Excel like cell navigation on any table. Events (focus, blur, action
                                        etc) can be assigned to individual cells, columns, rows or all cells.
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#add-modal">Add Homeroom +</a>
                                    </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Expertise</th>
                                            <th>Grade</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($getAllHomeroom as $gah) {?>
                                        <tr>
                                            <td><?=$gah->s_name ?></td>
                                            <td><?=$gah->name_expertise ?> | <?=$gah->alias_expertise ?></td>
                                            <td><?=$gah->grade ?></td>
                                            <th><a class="btn btn-teal" data-toggle="modal" data-target="#view-modal<?php echo $gah->id_homeroom ?>">
                                                    Options</a></th>
                                        </tr>
                                    <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>


<!-- Modal Add -->
<form action="<?php echo base_url(). 'Admin/addHomeroom' ?>" method="post" data-parsley-validate novalidate>
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Homeroom +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                 <label for="field-1" class="control-label">Name</label>
                                            <select name="id_user" class="form-control selectpicker" data-live-search="true" required >
                                                <option disabled="true" selected="selected">Teacher</option>
                                                <?php foreach ($getAllTeacher as $ga) { ?>
                                                    <option value="<?=$ga->id_user ?>"><?=$ga->s_name ?> | <?=$ga->username ?></option>
                                                <?php } ?>
                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Grade</label>
                                            <select name="grade" class="form-control" required>
                                                <option disabled="true" selected="selected">Grade</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Expertise</label>
                                            <select name="id_expertise" class="form-control selectpicker" data-live-search="true" required >
                                                <option disabled="true" selected="selected">Expertise</option>
                                                <?php foreach ($getExpertise as $ge) { ?>
                                                    <option value="<?=$ge->id_expertise ?>"><?=$ge->name_expertise ?> | <?=$ge->alias_expertise ?></option>
                                                <?php } ?>
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->

    <?php foreach ($getAllHomeroom as $gh) { ?>
        <!-- Modal View -->
<form action="<?php echo base_url(). 'Admin/updHomeroom' ?>" method="post" data-parsley-validate novalidate>
    <div id="view-modal<?php echo $gh->id_homeroom ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title"><?=$gh->grade ?> - <?=$gh->alias_expertise ?></h4>
                                                    <input type="hidden" name="current_homeroom" value="<?=$gh->id_user ?>">
                                                    <input type="hidden" name="id_homeroom" value="<?=$gh->id_homeroom ?>">
                                                </div>
                                                <div class="modal-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                 <label for="field-1" class="control-label">Name</label>
                                            <select name="id_user" class="form-control selectpicker" data-live-search="true" required >
                                                <option selected="selected" value="<?=$gh->id_user ?>"><?=$gh->s_name ?> ( <?=$gh->username ?> )</option>
                                                <option disabled="true">Teacher</option>
                                                <?php foreach ($getAllTeacher as $ga) { ?>
                                                    <option value="<?=$ga->id_user ?>"><?=$ga->s_name ?> | <?=$ga->username ?></option>
                                                <?php } ?>
                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Grade</label>
                                            <select name="grade" class="form-control" disabled="disabled" required>
                                                <option selected="selected"><?=$gh->grade ?></option>
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Expertise</label>
                                            <select name="id_expertise" disabled="disabled" class="form-control selectpicker" required >
                                                <option selected="selected"><?=$gh->name_expertise ?> | <?=$gh->alias_expertise ?></option>
                                                
                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal View -->
    <?php } ?>

</html>