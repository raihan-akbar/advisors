
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php $datenow =  date('Y-d-m');?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
                        
                        <!-- Start Content -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>User List</b></h4>
                                    <p class="text-muted font-13 m-b-30">
                                        Admin can View or Edit User<small>(Advisors/Teacher/Homeroom/Admin)</small> from this page.
                                    </p>

                                    <div class="text-right">
                                    <a class="btn btn-info"  data-toggle="modal" data-target="#modal-add">Add User +</a>
                                </div>

                                    <table id="datatable-keytable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Since</th>
                                            <th>View</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($getUserList as $i) {?>
                                        <tr>
                                            <td><?=$i->s_name ?></td>
                                            <td><?=$i->username ?></td>
                                            <td><?=$i->email ?></td>
                                            <td><?=$i->name_role ?></td>
                                            <td><?= substr($i->since, 0,-8); ?></td>
                                            <td><a class="btn btn-teal" data-toggle="modal" data-target="#modal-view<?php echo $i->id_user ?>">View</a></td>
                                        </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- End Content -->
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>

    <!-- Modal View -->
    <?php foreach ($getUserList as $o) {?>
    <form action="<?php echo base_url(). 'Admin/updUser' ?>" method="post" data-parsley-validate novalidate>
        <div id="modal-view<?php echo $o->id_user ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">View Data</h4>
                                                    <input type="hidden" name="id_user" value="<?php echo $o->id_user ?>">
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group no-margin">
                                                                    <div class="thumbnail">
                                                                    <img width="200px" src="<?=base_url('images/_advisors/'.$o->user_avatar) ?>" class="img-rounded img-responsive" alt="image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label  class="control-label">Name</label>
                                                                <input type="text" class="form-control" placeholder="Full Name" name="s_name" value="<?php echo $o->s_name ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label  class="control-label">Username</label>
                                                                <input type="text" class="form-control" name="username" value="<?=$o->username ?>" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label  class="control-label">Date Register</label>
                                                                <input disabled="disable" type="text" class="form-control" name="since" value="<?= substr($o->since, 0,-8); ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="email" value="<?=$o->email ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           <label class="control-label">Role</label>
                                                                <select name="id_role" class="form-control" required>
                                                                    <option selected value="<?=$o->id_role ?>"><?=$o->name_role ?></option>
                                                                    <?php foreach ($getRoleList as $r) { ?>
                                                                    <option value="<?=$r->id_role ?>"><?=$r->name_role ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="text-left">
                                                    <a style="color: red;" onclick="del(<?=$o->id_user ?>)" data-dismiss="modal">Delete User</a>
                                                </div>
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>
<?php } ?>

<!-- Modal Add -->
    <form action="<?php echo base_url(). 'Admin/addUser' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
        <div id="modal-add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add Data</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label  class="control-label">Name</label>
                                                                <input type="text" class="form-control" placeholder="Full Name" name="s_name" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label  class="control-label">Username</label>
                                                                <input id="username" type="text" class="form-control" name="username" placeholder="Username" required>
                                                            </div>
                                                        </div>
                                                </div>

                                                        <div class="row">
                                                        
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Password</label>
                                                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-Type Password</label>
                                                                    <input type="password" name="password" class="form-control" placeholder="Re-Type Password" required data-parsley-equalto="#password">
                                                                </div>
                                                            </div>
                                                        
                                                        </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           <label class="control-label">Role</label>
                                                                <select name="id_role" class="form-control" required>
                                                                    <option selected disabled="disabled">Select Role</option>
                                                                    <?php foreach ($getRoleList as $r) { ?>
                                                                    <option value="<?=$r->id_role ?>"><?=$r->name_role ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">User Avatar</label>
                                                            <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="text-left">
                                                    <a style="color: red;" onclick="del(<?=$o->id_user ?>)" data-dismiss="modal">Delete User</a>
                                                </div>
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>

<script>
$(document).ready(function(){
 $('form').parsley();
});
</script>

<script>//Swal Delete Offense
     function del(id) {
         swal({
  title: "Delete User?",
  text: "Your will not be able to recover this User Data! \n WE DON`T RECCOMEND \n because user data Connected to Another data.",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Admin/delUser/';?>"+id;

});

     }
 </script>

<?= $this->session->flashdata('notify'); ?>
</html>