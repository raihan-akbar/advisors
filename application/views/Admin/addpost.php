
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php foreach ($detailUser as $get) {} ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

                 <form  action="<?php echo base_url(). 'Admin/posting' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">

                    <div class="row">
                            <div class="col-sm-12">
                                    <div class="form-group text-right">
                                    <button type="submit" class="btn btn-primary"><h4 style="color: white">Add Post <i class="fa fa-plus"></i></h4></button>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">

                                    <div class="row">
                                        <div class="form-group">
                                                        <label>Title</label>
                                                        <div class="input-group col-sm-8">
                                                            <input required id="" name="blog_title" type="text" class="form-control" placeholder="Type Title Post">
                                                        </div><!-- input-group -->
                                         </div>

                                         <div class="form-group">
                                                       <label>Thumbnail / Cover Image</label>
                                                        <div class="input-group">
                                                        <input required type="file" class="filestyle" name="photo" data-buttontext="Select Image" data-buttonname="btn-default">
                                                        <code>Recommendation Size : 3000<small>px</small> x 1800<small>px</small> (Landscape)</code>
                                                        </div><!-- input-group -->
                                         </div>

                                          <div class="form-group col-lg-6">
                                                        <label>Category</label>
                                                        <select required name="blog_type" class="selectpicker" data-live-search="true" data-style="btn-default">
                                                            <option selected="selected" disabled="disabled" title="Select Category">Category</option>
                                                            <option value="News" title="News">News</option>
                                                            <option value="Event" title="Event">Event, Schedule, Annoucement</option>
                                                            <option value="Tech" title="Tech">Tech, Widget, Update</option>
                                                            <option value="Tips" title="Tips & Trick">Tips & Trick, Procedure, LifeHacks</option>
                                                             <option value="Fun" title="Fun">Fun, Thread, Unique, other</option>
                                                        </select>
                                         </div>

                                          <div class="form-group col-lg-8">
                                                        <small><?=$get->s_name ?> | <?=date('d-m-Y') ?></small>  
                                         </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <label>Text Content</label>
                                        <textarea required id="elm1" name="blog_content"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>
</html>