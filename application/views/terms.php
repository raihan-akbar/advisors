<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>PASIM +</title>

  <?php include '_source/landing_top.php'; ?>

</head>

<body>

	<div class="body-inner">

<!-- Header start -->
<header id="header" class="fixed-top header3" role="banner">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light px-4 py-2">
			<a class="navbar-brand" href="index.html"><img style="width: 166px" class="img-fluid" src="<?php echo base_url('assets/landing/images/cover-tag.png')?>" alt="logo"></a>
			<button class="navbar-toggler ml-auto border-0 rounded-0 text-dark" type="button" data-toggle="collapse"
				data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse text-center" id="navigation">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown active">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Home
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="index.html">Homepage 1</a>
							<a class="dropdown-item" href="index-2.html">Homepage 2</a>
							<a class="dropdown-item" href="index-3.html">Homepage 3</a>
							<a class="dropdown-item" href="index-4.html">Homepage 4</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Company
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="about.html">About Us</a>
							<a class="dropdown-item" href="service.html">Services</a>
							<a class="dropdown-item" href="career.html">Career</a>
							<a class="dropdown-item" href="testimonial.html">Testimonials</a>
							<a class="dropdown-item" href="faq.html">Faq</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Portfolio
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="portfolio-classic.html">Portfolio Classic</a>
							<a class="dropdown-item" href="portfolio-static.html">Portfolio Static</a>
							<a class="dropdown-item" href="portfolio-item.html">Portfolio Single</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Pages
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="team.html">Our Team</a>
							<a class="dropdown-item" href="about2.html">About Us - 2</a>
							<a class="dropdown-item" href="service2.html">Services - 2</a>
							<a class="dropdown-item" href="service-single.html">Services Single</a>
							<a class="dropdown-item" href="pricing.html">Pricing Table</a>
							<a class="dropdown-item" href="404.html">404 Page</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Blog
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="blog-rightside.html">Blog with Sidebar</a>
							<a class="dropdown-item" href="blog-item.html">Blog Single</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Features
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="typography.html">Typography</a>
							<a class="dropdown-item" href="elements.html">Elements</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">Contact</a></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="login">Login</a></a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<!--/ Header end -->
<!-- About tab start -->
<section id="about" class="about">
	<div class="container">
		<div class="row">
			<div class="col-md-12 heading text-center">
				<h2 class="title2">TERMS
					<span class="title-desc">I Have Terms Hahaha</span>
				</h2>
			</div>
		</div> <!-- Title row end -->
		<!-- featured tab -->
		<div class="row featured-tab">
			<div class="col-md-3 col-sm-5">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="animated fadeIn nav-link mb-1 active d-flex align-items-center" data-toggle="pill" href="#tab_1"
						role="tab" aria-selected="true">
						<i class="fa fa-info-circle mr-4 h3 mb-0"></i>
						<span class="h4 mb-0 font-weight-bold">Basic</span>
					</a>
					<a class="animated fadeIn nav-link mb-1 d-flex align-items-center" data-toggle="pill" href="#tab_2" role="tab"
						aria-selected="true">
						<i class="fa fa-handshake-o mr-4 h3 mb-0"></i>
						<span class="h4 mb-0 font-weight-bold">Commitment</span>
					</a>
					<a class="animated fadeIn nav-link mb-1 d-flex align-items-center" data-toggle="pill" href="#tab_3" role="tab"
						aria-selected="true">
						<i class="fa fa-android mr-4 h3 mb-0"></i>
						<span class="h4 mb-0 font-weight-bold">What We Do</span>
					</a>
					<a class="animated fadeIn nav-link mb-1 d-flex align-items-center" data-toggle="pill" href="#tab_4" role="tab"
						aria-selected="true">
						<i class="fa fa-pagelines mr-4 h3 mb-0"></i>
						<span class="h4 mb-0 font-weight-bold">Why choose us</span>
					</a>
					<a class="animated fadeIn nav-link mb-1 d-flex align-items-center" data-toggle="pill" href="#tab_5" role="tab"
						aria-selected="true">
						<i class="fa fa-support mr-4 h3 mb-0"></i>
						<span class="h4 mb-0 font-weight-bold">Save time with us</span>
					</a>
				</div>
			</div>
			<div class="col-md-9 col-sm-7">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane pl-sm-5 fade show active animated fadeInLeft" id="tab_1" role="tabpanel">
						<h3>What is Terms ?</h3>
						<p>Terms is provisions in an <strong>Agreement</strong> in the form of certain terms, conditions and guarantees that must be <strong>fulfilled by both parties.</strong></p>
						<p>Users must be able to <strong>Appreciate</strong> what has been made <strong>by the Programmer</strong> if you want to use this web app.</p>
						<p>This Website uses <strong>additional code</strong> or <strong>plugins</strong> from <strong>third parties</strong> to facilitate website development.</p>

						<h4>What i Do for this Web App?</h4>
						<ul class="check-list">
							<li><i class="fa fa-check"></i> Research</li>
							<li><i class="fa fa-check"></i> Make a Feature for User</li>
							<li><i class="fa fa-check"></i> Creating a Landing Page for Visitor</li>
							<li><i class="fa fa-check"></i> Create a Website System and Database Structure</li>
						</ul>
					</div>

					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_2" role="tabpanel">
						<h3>Your commitment</h3>
						<p>In return for our commitment to provide services, we require you to make the commitments below with us. </p>
						<ul class="check-list">
							<li><i class="fa fa-shield"></i> <strong>Not For Sale</strong><p>This Website and also the Scripts on this website are strictly forbidden to be traded.</p></li>
							<li><i class="fa fa-shield"></i> <strong>Permission</strong><p>It is forbidden to USE the script on this website And this website without the author's agreement.<br>It is forbidden to CHANGE the script and structure on this website without the author's agreement.</p></li>
							<li><i class="fa fa-shield"></i> <strong>Misuse</strong><p>Don't Misuse the Website.<br>Use the website with responsibly.<br>The programmer cannot be held responsible if the website is misused.</p></li>
							<li><i class="fa fa-shield"></i> <strong>Appreciate and Respect</strong> <p>Appreciate the programmer and other 
							Supporting party who has made this web app.</p></li>
						</ul>
						<br>
						<div class="strong">
						<h5>With the above commitment, use the website well, not to be traded and respect the programmers who have made this website. </h5>
						</div>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_3" role="tabpanel">
						<h3 class="text-center">We Build Ready made Web Applications for You</h3>
						<img class="img-fluid" src="<?php echo base_url('assets/landing/images/tab/featured-tab3.png')?>" alt="">
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_4" role="tabpanel">
						<h3>We Build Ready made Web Applications for You</h3>
						<ul class="check-list">
							<li><i class="fa fa-check"></i> Duis autem vel eum iriure</li>
							<li><i class="fa fa-check"></i> vulputate molestie consequat</li>
							<li><i class="fa fa-check"></i> Lorem ipsum dolor sit amet</li>
							<li><i class="fa fa-check"></i> Typi non habent claritatem insitam</li>
							<li><i class="fa fa-check"></i> Nam liber tempor cum soluta nobi</li>
							<li><i class="fa fa-check"></i> Nam liber tempor cum soluta nobi</li>
							<li><i class="fa fa-check"></i> Typi non habent claritatem insitam</li>
							<li><i class="fa fa-check"></i> vulputate molestie consequat</li>
						</ul>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_5" role="tabpanel">
						<i class="fa fa-coffee float-left icon-xl"></i>
						<h3>Easy Solution on Every Project</h3>
						<p>Occupy selfies Tonx, +1 Truffaut beard organic normcore tilde flannel artisan squid cray single-origin
							coffee. Master cleanse vinyl Austin kogi, semiotics skateboard fap wayfarers health goth. Helvetica cray
							church-key hashtag Carles. Four dollar toast meggings seitan, Tonx pork belly VHS Bushwick. Chambray banh
							mi cornhole. Locavore messenger bag seitan.</p>
						<p>Helvetica cold-pressed lomo messenger bag ugh. Vinyl jean shorts Austin pork belly PBR retro, Etsy VHS
							kitsch actually messenger bag pug. semiotics try-hard, Schlitz occupy dreamcatcher master cleanse Marfa
							Vice tofu. </p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Container end -->
</section><!-- About end -->

<!-- Newsletter start -->
<section id="newsletter" class="newsletter">
	<div class="container">
		<div class="row">
			<div class="col-md-12 heading text-center">
				<span class="icon-pentagon wow bounceIn animated"><i class="fa fa-envelope"></i></span>
				<h2 class="title2">Subscribe With Us
					<span class="title-desc">We Love to Work with Passion</span>
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 mx-auto">
				<form action="#" method="post" id="newsletter-form" class="newsletter-form wow bounceIn"
					data-wow-duration=".8s">
					<div class="form-group">
						<input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg"
							placeholder="Enter your email address" autocomplete="off">
						<button class="btn btn-primary solid">Subscribe</button>
					</div>
				</form>
			</div>
		</div>
		<!--/ Content row end -->
	</div>
	<!--/ Container end -->
</section><!-- Newsletter end -->


<!-- Map start here -->
<section id="map-wrapper" class="p-0">
	<div class="container">
		<div class="contact-info-inner">
			<h3>Contact Info</h3>
			<div><i class="fa fa-map-marker float-left"></i>
				<p><strong>Address</strong>1102 Saint Marys, Junction City, KS</p>
			</div>
			<div><i class="fa fa-phone float-left"></i>
				<p><strong>Phone</strong>+(785) 238-4131</p>
			</div>
			<div><i class="fa fa-envelope-o float-left"></i>
				<p><strong>Email</strong>info@spikeinco.com</p>
			</div>
			<div><i class="fa fa-compass float-left"></i>
				<p><strong>Office Hours</strong>Mon - Friday, 9:00 - 5:00</p>
			</div>

		</div>
	</div>
	<div class="map" id="map_canvas" data-latitude="51.507351" data-longitude="-0.127758" data-marker="<?php echo base_url('assets/landing/images/marker.png')?>"></div>
</section>
<!--/ Map end here -->


<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img src="<?php echo base_url('assets/landing/images/logo.png')?>" alt="logo">
				</div>
				<div class="gap-20"></div>
				<ul class="dark unstyled">
					<li>
						<a title="Twitter" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
						</a>
						<a title="Facebook" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
						</a>
						<a title="Google+" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
						</a>
						<a title="linkedin" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
						</a>
						<a title="Pinterest" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
						</a>
						<a title="Skype" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
						</a>
						<a title="Dribble" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; Copyright 2019 Themefisher. <span>Designed by <a
							href="https://themefisher.com">Themefisher.com</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->

</div><!-- Body inner end -->

<?php include '_source/landing_bottom.php'; ?>

</body>

</html>