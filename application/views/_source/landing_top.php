<!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/landing/plugins/bootstrap/bootstrap.min.css')?>">
	<!-- FontAwesome -->
  <link rel="stylesheet" href="<?= base_url('assets/landing/plugins/fontawesome/font-awesome.min.css')?>">
	<!-- Animation -->
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/animate.css')?>">
	<!-- Prettyphoto -->
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/prettyPhoto.css')?>">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/owl/owl.carousel.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/owl/owl.theme.css')?>">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/flex-slider/flexslider.css')?>">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?= base_url('assets/landing/plugins/cd-hero/cd-hero.css')?>">
	<!-- Style Swicther -->
	<link id="style-switch" href="<?= base_url('assets/landing/css/presets/preset3.css')?>" media="screen" rel="stylesheet" type="text/css">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="plugins/html5shiv.js"></script>
      <script src="plugins/respond.min.js"></script>
    <![endif]-->

  <!-- Main Stylesheet -->
  <link href="<?= base_url('assets/landing/css/style.css')?>" rel="stylesheet">
  
  <!--Favicon-->
	<link rel="icon" href="<?= base_url('assets/landing/img/favicon/favicon-32x32.png')?>" type="image/x-icon" />