<!-- jQuery -->
<script src="<?= base_url('assets/landing/plugins/jQuery/jquery.min.js')?>"></script>
<!-- Bootstrap JS -->
<script src="<?= base_url('assets/landing/plugins/bootstrap/bootstrap.min.js')?>"></script>
<!-- Style Switcher -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/style-switcher.js')?>"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/owl/owl.carousel.js')?>"></script>
<!-- PrettyPhoto -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/jquery.prettyPhoto.js')?>"></script>
<!-- Bxslider -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/flex-slider/jquery.flexslider.js')?>"></script>
<!-- CD Hero slider -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/cd-hero/cd-hero.js')?>"></script>
<!-- Isotope -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/isotope.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/ini.isotope.js')?>"></script>
<!-- Wow Animation -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/wow.min.js')?>"></script>
<!-- Eeasing -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/jquery.easing.1.3.js')?>"></script>
<!-- Counter -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/jquery.counterup.min.js')?>"></script>
<!-- Waypoints -->
<script type="text/javascript" src="<?= base_url('assets/landing/plugins/waypoints.min.js')?>"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="<?= base_url('assets/landing/plugins/google-map/gmap.js')?>"></script>

<!-- Main Script -->
<script src="<?= base_url('assets/landing/js/script.js')?>"></script>
