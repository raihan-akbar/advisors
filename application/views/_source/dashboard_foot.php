<!-- jQuery  -->
        <script src="<?php echo base_url('assets/dashboard/js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/detect.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/fastclick.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/jquery.blockUI.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/waves.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/jquery.slimscroll.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/switchery/switchery.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/parsleyjs/parsley.min.js')?>"></script>

        <!-- Counter js  -->
        <script src="<?php echo base_url('assets/dashboard/plugins/waypoints/jquery.waypoints.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/counterup/jquery.counterup.min.js')?>"></script>

        <!--Morris Chart-->
                <script src="<?php echo base_url('assets/dashboard/plugins/morris/morris.min.js')?>"></script>
                <script src="<?php echo base_url('assets/dashboard/plugins/raphael/raphael-min.js')?>"></script>
                <script src="<?php echo base_url('assets/dashboard/pages/jquery.morris.init.js')?>"></script>

        <!-- Dashboard init -->
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.dashboard.js')?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets/dashboard/js/jquery.core.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/js/jquery.app.js')?>"></script>

        <!-- Modal-Effect -->
        <script src="<?php echo base_url('assets/dashboard/plugins/custombox/js/custombox.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/custombox/js/legacy.min.js')?>"></script>

        <!-- responsive-table-->
        <script src="<?php echo base_url('assets/dashboard/plugins/responsive-table/js/rwd-table.min.js')?>" type="text/javascript"></script>


    <!-- DAta Table-->
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/jquery.dataTables.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.bootstrap.js')?>"></script>

        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.buttons.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/buttons.bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/jszip.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/pdfmake.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/vfs_fonts.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/buttons.html5.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/buttons.print.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.fixedHeader.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.keyTable.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.responsive.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/responsive.bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.scroller.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.colVis.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.fixedColumns.min.js')?>"></script>

        <!-- Sweet-Alert  -->
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.sweet-alert.init.js')?>"></script>

<!-- init -->
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.datatables.init.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.form-pickers.init.js')?>"></script>
<!-- Toastr js -->
        <script src="<?php echo base_url('assets/dashboard/plugins/toastr/toastr.min.js')?>"></script>
        <!-- Toastr init js (Demo)-->
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.toastr.js')?>"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable({keys: true});
                $('#datatable-responsive').DataTable();
                $('#datatable-colvid').DataTable({
                    "dom": 'C<"clear">lfrtip',
                    "colVis": {
                        "buttonText": "Hi"
                    }
                });
                $('#datatable-scroller').DataTable({
                    ajax: "<?php echo base_url('assets/dashboard/plugins/datatables/json/scroller-demo.json')?>",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                var table = $('#datatable-fixed-col').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 1
                    }
                });
            });
            TableManageButtons.init();

        </script>

<!-- KNOB JS -->
        
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/jquery-knob/excanvas.js')?>"></script>

        <!-- Jquery filer js -->
        <script src="<?php echo base_url('assets/dashboard/plugins/jquery.filer/js/jquery.filer.min.js')?>"></script>

        <!-- page specific js -->
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.fileuploads.init.js')?>"></script>


<!-- SelectPICKER -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dashboard/select-picker/dist/css/bootstrap-select.min.css')?>">
    <script src="<?php echo base_url('assets/dashboard/select-picker/dist/js/bootstrap-select.min.js') ?>"></script>

<!-- Date & Time Picker -->
       <script src="<?php echo base_url('assets/dashboard/plugins/moment/moment.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/timepicker/bootstrap-timepicker.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js')?>"></script>

<!--Form Wizard-->
        <script src="<?php echo base_url('assets/dashboard/plugins/jquery.steps/js/jquery.steps.min.js')?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/jquery-validation/js/jquery.validate.min.js')?>"></script>

<!--wizard initialization-->
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.wizard-init.js')?>" type="text/javascript"></script>

<!-- Advanced Form -->

        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/multiselect/js/jquery.multi-select.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/jquery-quicksearch/jquery.quicksearch.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/select2/js/select2.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-select/js/bootstrap-select.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/dashboard/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')?>" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/autocomplete/jquery.mockjax.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/autocomplete/jquery.autocomplete.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/plugins/autocomplete/countries.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/pages/jquery.autocomplete.init.js')?>"></script>

        <script type="text/javascript" src="<?php echo base_url('assets/dashboard/pages/jquery.form-advanced.init.js')?>"></script>


        <!-- Tooltipster js -->
        <script src="<?php echo base_url('assets/dashboard/plugins/tooltipster/tooltipster.bundle.min.js')?>"></script>
        <script src="<?php echo base_url('assets/dashboard/pages/jquery.tooltipster.js')?>"></script>

        <!--Summernote js-->
        <script src="<?php echo base_url('assets/dashboard/plugins/summernote/summernote.min.js')?>"></script>

        <script>

            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 350,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });

                $('.inline-editor').summernote({
                    airMode: true
                });

            });
        </script>

        <!--Wysiwig js-->
        <script src="<?php echo base_url('assets/dashboard/plugins/tinymce/tinymce.min.js')?>"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                if($("#elm1").length > 0){
                    tinymce.init({
                        selector: "textarea#elm1",
                        theme: "modern",
                        height:300,
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                            "save table contextmenu directionality emoticons template paste textcolor"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink | print preview fullpage | forecolor backcolor",
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ]
                    });
                }
            });
        </script>
        