<!-- Table Responsive css -->
        <link href="<?php echo base_url('assets/dashboard/plugins/responsive-table/css/rwd-table.min.css')?>" rel="stylesheet" type="text/css" media="screen">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/dashboard/plugins/morris/morris.css')?>">

        <!-- App css -->
        <link href="<?php echo base_url('assets/dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/core.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/components.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/icons.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/pages.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/menu.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('assets/dashboard/css/responsive.css" rel="stylesheet" type="text/css')?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/dashboard/plugins/switchery/switchery.min.css')?>">

        <!-- Plugins css-->
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/dashboard/plugins/multiselect/css/multi-select.css')?>"  rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/dashboard/plugins/select2/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-select/css/bootstrap-select.min.css')?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet" />


        <script src="<?php echo base_url('assets/dashboard/js/modernizr.min.js')?>"></script>

        <!-- DataTables -->
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/jquery.dataTables.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/buttons.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/fixedHeader.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/responsive.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/scroller.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.colVis.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/dataTables.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('assets/dashboard/plugins/datatables/fixedColumns.dataTables.min.css')?>" rel="stylesheet" type="text/css')?>"/>

        <!-- Sweet Alert -->
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-sweetalert/sweet-alert.css')?>" rel="stylesheet" type="text/css">

        <!-- Notification css (Toastr) -->
        <link href="<?php echo base_url('assets/dashboard/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />

        <!-- Jquery filer css -->
        <link href="<?php echo base_url('assets/dashboard/plugins/jquery.filer/css/jquery.filer.css')?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/dashboard/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')?>" rel="stylesheet" />

        <!-- Date & Time Picker -->
        <link href="<?php echo base_url('assets/dashboard/plugins/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/dashboard/plugins/clockpicker/css/bootstrap-clockpicker.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/dashboard/plugins/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet">


        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dashboard/plugins/jquery.steps/css/jquery.steps.css')?>" />

        <!-- Custom box css -->
        <link href="<?php echo base_url('assets/dashboard/plugins/custombox/css/custombox.min.css')?>" rel="stylesheet">

        <!-- Tooltipster css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/dashboard/plugins/tooltipster/tooltipster.bundle.min.css')?>">

        <!-- Summernote css -->
        <link href="<?php echo base_url('assets/dashboard/plugins/summernote/summernote.css')?>" rel="stylesheet" />

