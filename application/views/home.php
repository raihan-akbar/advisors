<!DOCTYPE html>
<html lang="">

<head>
  <meta charset="utf-8">
  <title>PASIM +</title>

  <?php include '_source/landing_top.php'; ?>

</head>

<body>

	<div class="body-inner">

<!-- Header start -->
<header id="header" class="fixed-top header2" role="banner">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-dark">
			<a class="navbar-brand" href="index.html"><img class="img-fluid" style="width: 145px"  src="<?=base_url('assets/img/tag-2.png') ?>" alt="logo"></a>
			<button class="navbar-toggler ml-auto border-0 rounded-0 text-white" type="button" data-toggle="collapse"
				data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse text-center" id="navigation">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="contact.html">About</a></a>
					</li>
					<li class="nav-item dropdown active">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
							Achievement
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="index.html"><strong>Student</strong></a>
							<a class="dropdown-item" href="index-2.html"><strong>School</strong></a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">Expertise</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">History</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url('blog/') ?>">Blog</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">Contact</a>
					</li>
					<li class="nav-item">
						<?php if ($this->session->userdata('condition') == 'Online') {
							$con = $this->session->userdata('con');
							$item = '<a class="nav-link" href="'.$con.'/">Dashboard</a>';
							}else{$item = '<a class="nav-link" href="login/">Sign-in</a>';$con = '';}
							?>
						<?= $item; ?>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<!--/ Header end -->

<!-- Slider start -->
<section id="home" class="p-0">
	<div id="main-slide" class="ts-flex-slider">
		<div class="flexSlideshow flexslider">
			<ul class="slides">
				<li>
					<div class="overlay2">
						<img class=""  src="<?= base_url('assets/landing/images/slider/slide-1.jpg')?>" alt="slider">
					</div>
					<div class="flex-caption slider-content">
						<div class="col-md-12 text-center">
							<h2 class="animated2">
								SMK PASIM PLUS
							</h2>
							<h3 class="animated3">
								Creative School
							</h3>
						</div>
						<p class="animated4"><a href="#plan" class="slider btn btn-primary solid" >Explore</a></p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!--/ Main slider end -->
</section>

<section id="plan" class="about angle">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="wow fadeIn" data-wow-duration="1s">
				<div class="col-md-6 heading text-center">
				<span class="icon-pentagon wow bounceIn"><i class="fa fa-compass"></i></span>
				<h3 class="title2">Strategic Plan
					<span class="title-desc">Sharpen Student Skills</span>
				</h3>
			</div>

				</div>

			</div>
			<div class="col-md-6">
				<div class="video-block-head">
					<h3>Skill</h3>
					<p>Our Institute puts forward two things that really help students to become someone useful, namely Intelligence and Creativity.
					</p>
				</div>

				<div class="video-block-content">
					<span class="feature-icon float-left"><i class="fa fa-diamond"></i></span>
					<div class="feature-content">
						<h3>Intelligence</h3>
						<p>Educate students to solve problems with logic and intelligence</p>
					</div>
				</div>
				<!--/ End 1st block -->

				<div class="video-block-content">
					<span class="feature-icon float-left"><i class="fa fa-magic"></i></span>
					<div class="feature-content">
						<h3>Creativity</h3>
						<p>Sharpen student creativity to make or process something that can be useful for students or their surroundings</p>
					</div>

				</div>
				<!--/ End 1st block -->

			</div>
		</div>
	</div>
</section>

<!-- Portfolio start -->
<section id="portfolio" class="about portfolio-box">
	<div class="container">
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon classic float-left"><i class="fa fa-image"></i></span>
				<h2 class="title classic">Student Activity</h2>
			</div>
		</div> <!-- Title row end -->

		<div id="isotope" class="row isotope">
			<div class="col-sm-3 web-design isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio1.jpg')?>" alt="">
						<figcaption>
							<h3>Startup Business</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg1.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 development isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio2.jpg')?>" alt="">
						<figcaption>
							<h3>Easy to Lanunch</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg2.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 joomla isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio3.jpg')?>" alt="">
						<figcaption>
							<h3>Your Business</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg3.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio4.jpg')?>" alt="">
						<figcaption>
							<h3>Prego Match</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg2.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 joomla isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio5.jpg')?>" alt="">
						<figcaption>
							<h3>Fashion Brand</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg3.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 development isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio6.jpg')?>" alt="">
						<figcaption>
							<h3>The Insidage</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg1.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 development isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio7.jpg')?>" alt="">
						<figcaption>
							<h3>Light Carpet</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg2.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->

			<div class="col-sm-3 development isotope-item">
				<div class="grid">
					<figure class="m-0 effect-oscar">
						<img src="<?=base_url('assets/landing/images/portfolio/portfolio8.jpg')?>" alt="">
						<figcaption>
							<h3>Amazing Keyboard</h3>
							<a class="view icon-pentagon" data-rel="prettyPhoto" href="<?=base_url('assets/landing/images/portfolio/portfolio-bg3.jpg')?>"><i
									class="fa fa-search"></i></a>
						</figcaption>
					</figure>
				</div>
			</div><!-- Isotope item end -->
		</div><!-- Content row end -->
	</div><!-- Container end -->
</section><!-- Portfolio end -->

<!-- Main container start -->
<section id="main-container">
	<div class="container">
		<div class="gap-60"></div>

		<!-- Pricing table start -->
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon classic float-left"><i class="fa fa-newspaper-o"></i></span>
				<h2 class="title classic">News and Event</h2>
			</div>
		</div><!-- Title row end -->


		<div class="row">
			<!-- plan start -->

			<!-- Start ForEach Row Here -->
			<?php foreach ($topThree as $get) { ?>

			<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay=".5s">
				<div class="plan">
					<img class="col-lg-12" src="<?=base_url('images/_blog/'.$get->blog_img)?>" alt="cover">
					<ul class="list-unstyled">
						<li><h4><?=$get->blog_title ?></h4></li>
						<li><i class="fa fa-calendar-minus-o"></i> <?=$get->blog_date ?><br>
							<i class="fa fa-pie-chart"></i> <?=$get->blog_type ?></li>
					</ul>
					<div class="text-center">
					<a class="btn btn-primary" href="<?=base_url('controller/url_blog/') ?>">Read More</a>
					</div>
				</div>
			</div><!-- plan end -->
		<?php } ?>
			<!-- End ForEach Here -->

		</div>
		<!--/ Content row end -->
	</div><!-- container end -->
</section>
<!--/ Main container end -->

<!-- Parallax 1 start -->
<section class="parallax parallax1">
	<div class="parallax-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Read More News and Event</h3>
				<a href="<?=base_url('blog') ?>" class="btn btn-primary">Go to Blog SIte</a>
			</div>
		</div>
	</div><!-- Container end -->
</section><!-- Parallax 1 end -->


<!-- Testimonial start-->
<section class="testimonial parallax parallax3">
	<div class="parallax-overlay"></div>
	<div class="container">
		<div class="row">
			<div id="testimonial-carousel" class="owl-carousel owl-theme text-center testimonial-slide">
				<div class="item">
					<div class="testimonial-content">
						<p class="testimonial-text">
							Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations.
						</p>
						<h3 class="name">Steve Jobs<span>Computer Revolutioner</span></h3>
					</div>
				</div>
				<div class="item">
					<div class="testimonial-content">
						<p class="testimonial-text">
							Nothing is too wonderful to be true if it be consistent with the laws of nature.
						</p>
						<h3 class="name">Michael Faraday<span>Scientist</span></h3>
					</div>
				</div>
				<div class="item">
					<div class="testimonial-content">
						<p class="testimonial-text">
							Sometimes you fail and fall, but if you want You can Fall in Style.
						</p>
						<h3 class="name">Raihan Akbar<span>Architect</span></h3>
					</div>
				</div>
			</div>
			<!--/ Testimonial carousel end-->
		</div>
		<!--/ Row end-->
	</div>
	<!--/  Container end-->
</section>
<!--/ Testimonial end-->


<!-- Counter Strat -->
<section class="ts_counter p-0">
	<div class="container-fluid">
		<div class="row facts-wrapper wow fadeInLeft text-center">
			<div class="facts one col-md-6 col-sm-6">
				<span class="facts-icon"><i class="fa fa-user"></i></span>
				<div class="facts-num">
					<span class="counter">1200</span>
				</div>
				<h3>Student</h3>
			</div>

			<div class="facts two col-md-6 col-sm-6">
				<span class="facts-icon"><i class="fa fa-institution"></i></span>
				<div class="facts-num">
					<span class="counter">1277</span>
				</div>
				<h3>Alumni</h3>
			</div>

		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Counter end -->

<section id="featured-video" class="about">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="wow fadeIn" data-wow-duration="1s">
				<div class="col-md-6 heading text-center">
				<span class="icon-pentagon wow bounceIn"><i class="fa fa-compass"></i></span>
				<h3 class="title2">Contact Info
					<span class="title-desc">Social Media</span>
				</h3>
				<div class="team-social">
							<a class="fb" href="https://www.facebook.com/PasimCreativeSchool" target="_blank"><i class="fa fa-facebook"></i></a>
							<a class="gplus" href="https://www.youtube.com/channel/UCoZXfk5nMprxgyF9dMmzCsQ" target="_blank"><i class="fa fa-youtube"></i></a>
							<a class="gplus" href="https://www.google.com/maps/place/SMK+Pasim+Plus+Kota+Sukabumi/@-6.9137272,106.9381462,15z/data=!4m2!3m1!1s0x0:0x4d72d756efe7c219?sa=X&ved=2ahUKEwiS19bF3urnAhWSwTgGHSElBDUQ_BIwFnoECBkQCA" target="_blank"><i class="fa fa-google-plus"></i></a>
							<a class="dribble" href="https://www.instagram.com/ayokepasim/" target="_blank"><i class="fa fa-instagram"></i></a>
						</div>
			</div>

				</div>

			</div>
			<div class="col-md-6">
				<div class="video-block-head">
					<span class="feature-icon float-left"><i class="fa fa-map-marker"></i></span>
					<div class="feature-content">
					<h4>Address</h4>
					</div>
					<hr/>
					<p><strong>Perana Avenue, Cisarua<br>Sub-Dis. Cikole, Sukabumi Regency<br>West Java 43113.
					</strong></p>
				</div>

				<div class="video-block-content">
					<span class="feature-icon float-left"><i class="fa fa-phone"></i></span>
					<div class="feature-content">
						<p><strong>(0266) 241000</strong></p>
					</div>
				</div>
				<!--/ End 1st block -->

				<div class="video-block-content">
					<span class="feature-icon float-left"><i class="fa fa-envelope"></i></span>
					<div class="feature-content">
						<p><strong>smk@pasim.com</strong></p>
					</div>

				</div>
				<!--/ End 1st block -->

				<div class="video-block-content">
					<span class="feature-icon float-left"><i class="fa fa-compass"></i></span>
					<div class="feature-content">
						<p><strong>Weekday | 07.00 <small>-</small> 16.00 <br> Weekend | 08.00 <small>-</small> 14.00</strong></p>
					</div>

				</div>
				<!--/ End 1st block -->

			</div>
		</div>
	</div>
</section>

<!-- Map start here -->
<section id="map-wrapper" class="p-0">

<iframe id="map_canvas" class="border map col-md-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3987.49254236909!2d106.93612248397096!3d-6.915148596288713!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7b2ef13012216dd9!2sSmk%20Pasim%20Plus!5e0!3m2!1sid!2sid!4v1582563622468!5m2!1sid!2sid" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</section>
<!--/ Map end here -->

<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img width="20%" src="<?php echo base_url('assets/img/tag-2.png')?>" alt="logo">
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; <?=date('Y') ?> <span>System And Structure Designed  by <a href="https://instagram.com/raihan_cf" target="_blank">Raihan Akbar</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
<?php 
/*
<!-- Footer start -->
<section id="footer" class="footer footer-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="footer-logo">
					<img src="<?php echo base_url('assets/landing/images/logo.png')?>" alt="logo">
				</div>
				<div class="gap-20"></div>
				<ul class="dark unstyled">
					<li>
						<a title="Twitter" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
						</a>
						<a title="Facebook" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
						</a>
						<a title="Google+" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
						</a>
						<a title="linkedin" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
						</a>
						<a title="Pinterest" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
						</a>
						<a title="Skype" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
						</a>
						<a title="Dribble" href="#">
							<span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/ Row end -->
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright-info">
					&copy; Copyright 2019 Themefisher. <span>Designed by <a
							href="https://themefisher.com">Themefisher.com</a></span>
				</div>
			</div>
		</div>
		<!--/ Row end -->
		<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
			<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
		</div>
	</div>
	<!--/ Container end -->
</section>
<!--/ Footer end -->
*/
 ?>
</div><!-- Body inner end -->

<?php include '_source/landing_bottom.php'; ?>
<?=$this->session->flashdata('notify'); ?>

</body>

</html>