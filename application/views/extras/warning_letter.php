
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php date_default_timezone_set("Asia/Jakarta"); ?>
        <?php $datenow =  date('Y-d-m');?>
        <?php $dateshow = date('d F, Y') ?>
        <?php $nomonth = date('m') ?>
        <?php $noyear = date('Y') ?>

        <?php foreach ($getCatchLetter as $get) {}?>
        <?php foreach ($detailUser as $user) {} ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                           Letter Overview
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h3><img src="<?=base_url('assets/img/pasim-logo.png') ?>" height="100"></h3>
                                            </div>
                                            <div class="text-center">
                                            <h2>SMK PASIM PLUS KOTA SUKABUMI</h2>
                                            <p>Jl. Perana, Cikole, Kec. Cikole, Kota Sukabumi, Jawa Barat 43113</p>
                                            </div>
                                        </div>
                                        <hr style="border-width: 5px;"/>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="text-center">
                                                    <h3><?=$get->letter_title ?></h3>
                                                </div>

                                                <div class="pull-left m-t-15">
                                                    <p><strong>Nomor :</strong> R/XX/XX/<?=$nomonth;?>/<?=$noyear ?></p>
                                                    <p><strong>Lampiran :</strong>  -</p>
                                                    <p><strong>Perihal :</strong> <?=$get->concerning_subject ?></p>
                                                    <p><strong>Tanggal :</strong> <?=$get->date_show ?></p>
                                                </div>

                                                <div class="pull-right m-t-15">
                                                    <address>
                                                      Kepadat Yth.<br>
                                                      Bapak/Ibu Orang Tua Siswa dari<br><b>
                                                      <?=$get->name ?></b><br>
                                                      <abbr>di</abbr> Tempat
                                                      </address>
                                                </div>
                                               
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->
                                        <hr/>

                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <p>Dengan Hormat,</p>
                                                    <p>Sehubungan dengan sikap indisipliner serta pelanggaran tata tertib Sekolah yang dilakuakn oleh <b><?=$get->name ?></b>,</p>
                                                    <p>Surat Peringatan ini diberikan kepada : </p>
                                                    <br>
                                                    <p class="m-l-15 text font-13">Nama : <span class="m-l-15"><?php echo $get->name ?></span></p>
                                                    <p class="m-l-15 text font-13">NIS : <span class="m-l-15"><?php echo $get->nis ?></span></p>
                                                    <p class="m-l-15 text font-13">Kelas : <span class="m-l-15"><?php echo $get->grade ?></span></p>
                                                    <p class="m-l-15 text font-13">Jurusan : <span class="m-l-15"><?php echo $get->name_expertise ?> (<?=$get->alias_expertise ?>)</span></p>
                                                    <p>
                                                        Dengan ini Kami memberikan <b>Surat Peringatan Tertulis</b>. <br></p>
                                                        <p><?=$get->letter_reason ?></p>
                                                    <br>
                                                    <footer style="padding-top: 15%">
                                                        <div class="row">
                                                 <div class="col-md-12">
                                             <div class="pull-left m-t-15">
                                                <p class="text-inverse font-600"><u><b>Kepala Sekolah  </b></u></p>
                                            </div>

                                            <div class="pull-right m-t-15">
                                               <p class="text-inverse font-600"><u><b><?=$get->name ?>  </b></u></p>
                                            </div>
                                                 </div>
                                            </div>

                                        </div>
                                                    </footer>


                                        <hr class="hidden-print">
                                        </div>

                                        <?php if ($this->session->userdata('role') == 'Advisors' ) {
                                            $btnshow = '<div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Print</a>
                                                <a href="#view-letter" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a" class="btn btn-primary waves-effect waves-light">Edit Letter</a>
                                            </div>
                                        </div>';
                                        }else{$btnshow="";} ?>
                                            <?=$btnshow; ?>
                                        <div class="hidden-print">
                                            <div class="pull-left">
                                                <p>Author : <?=$get->s_name ?></p>
                                                <p>To : <?=$get->name ?></p>
                                                <p>Concerning : <?=$get->concerning_subject ?></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>
        <?php echo $this->session->flashdata('notify'); ?>

    </body>

    <!-- Modal Add Letter -->
<form action="<?php echo base_url('Master/updLetter'). '#' ?>" method="post" data-parsley-validate enctype="multipart/form-data">
    <div id="view-letter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Edit Letter</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="row">
                                                    <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input style="color: red" class="form-control" type="text" disabled="disabled" name="date_show" value="<?=$get->warned_type ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">

                                                    <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Title</label>
                                                                <div>
                                                                    <input type="text" name="letter_title" class="form-control" placeholder="Letter Title" required value="<?=$get->letter_title ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Concerning Subject</label>
                                                                <div>
                                                                    <input type="text" name="concerning_subject" class="form-control" placeholder="Concening Subjects" required value="<?=$get->concerning_subject ?>">
                                                                </div>
                                                            </div>
                                                     </div>

                                                    </div>

                                                    <div class="row">
                                                    <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Date Letter Show</label>
                                                                <input class="form-control" type="text" name="date_show" value="<?=$get->date_show ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Warning Reason</label>
                                                                <textarea class="form-control autogrow" placeholder="Explain About the Letter" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="letter_reason" required><?=$get->letter_reason ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <p>Please Read <a href=""><u>Letter Guide</u> </a> for Best Experience.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Save Letter</button>
                                                    <input type="hidden" name="id_student" value="<?php echo $get->id_student ?>">
                                                    <input type="hidden" name="id_user" value=" <?php echo $user->id_user ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>


<?= $this->session->flashdata('notify'); ?>
</html>