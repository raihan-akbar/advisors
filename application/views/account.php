
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Students</title>

        <?php $this->load->view('_source/dashboard_head.php') ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('_partials/dashboard_topbar.php') ?>
            <?php $this->load->view('_partials/dashboard_sidebar.php') ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Starter Page </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Pages </a>
                                        </li>
                                        <li class="active">
                                            Blank Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

        <!-- Start Content Here -->

<?php foreach ($detailUser as $get) {} ?>
<?php 
$since = substr($get->since, 0,-8);
$con = $this->session->userdata('con');
 ?>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumbnail">
                                                        <img width="200" src="<?= base_url('images/_advisors/'.$get->user_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                    </div>
                                                    <div class="">
                                                        <h4 class="m-b-5"></h4>
                                                    </div>

                                                    <a  href="#avatar-modal" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a" class="btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light">Change Picture</a>

                                                    <hr/>

                                                    <div class="text-left">
                                                        <a class="" href="#details-modal" data-animation="silde" data-plugin="modal" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">Change Account Information</a><hr/>
                                                        <a class="" href="#password-modal" data-animation="silde" data-toggle="modal" data-overlaySpeed="200" data-overlayColor="#36404a">Change Password</a><hr/>
                                                        <a class="" href="<?= base_url('Auth/logout')  ?>" style="color: red;"><b>SignOut</b> <i class="fa fa-sign-out"></i></a><hr/>
                                                        
                                                    </div>


                                                </div>

                                            </div> <!-- end card-box -->

                                        </div> <!-- end col -->

                                            <div class="col-md-8 col-lg-9">

                                            <div class="row">
                                                <div class="col-md-12 col-sm-6">
                                                    <div class="text-right">
                                                    
                                            </div>
                                            <h4>User Details</h4>
                                                    <hr/>
                                                    <p class="text font-13"><strong>Name :</strong> <span class="m-l-15"><?=$get->s_name  ?></span></p>
                                                        <p class="text font-13"><strong>Username :</strong> <span class="m-l-15"><?=$get->username  ?></span></p>
                                                        <p class="text font-13"><strong>Email :</strong> <span class="m-l-15"><?=$get->email  ?></span></p>
                                                        <p class="text font-13"><strong>Role :</strong> <span class="m-l-15"><?=$get->name_role  ?></span></p>
                                                        <p class="text font-13"><strong>Register :</strong> <span class="m-l-15"><?=$since  ?></span></p>

                                                        <hr/>

                                </div>
                            </div>
                        </div>


                        <!-- End row -->


        <!-- END CONTENT -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php $this->load->view('_source/dashboard_foot.php') ?>

    </body>

    <!-- Modal UpdAva -->
    <form action="<?php echo base_url($con). '/updAvaUser' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
    <div id="avatar-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Change Avatar</h4>
                                                    <input type="hidden" name="id_user" value="<?=$get->id_user ?>">
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Current Avatar</label>
                                                                <div class="thumbnail">
                                                                    <img width="200" src="<?= base_url('images/_advisors/'.$get->user_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">New Avatar</label>
                                                                <div class="">
                                                                    <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary" required>
                                                                     </div><hr>

                                                                     <div class="">
                                                                        <a onclick="del('<?=$get->user_avatar ?>')" data-dismiss="modal" style="color: red">Delete Current Avatar <i class="fa fa-trash"></i></a>
                                                                     </div><hr>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="submit-password" type="submit" class="btn btn-facebook waves-effect waves-light">Save </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                                </form>
<!-- Modal Ava -->

<!-- Modal Pass -->
    <form action="<?php echo base_url($con). '/updPassword' ?>" method="post" data-parsley-validate novalidate>
    <div id="password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Change Password</h4>
                                                    <input type="hidden" name="id_user" value="<?=$get->id_user ?>">
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">Current Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="old" name="old" class="form-control" placeholder="Your Current Password" required>
                                                                <span class="input-group-addon btn-teal"  onclick="change()"  id="see">
                                                                    Show
                                                                </span>
                                                            </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">New Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="password" name="password" class="form-control" placeholder="Your Old Password" required data-parsley-minlength="8">
                                                                <span class="input-group-addon"  id="v1">
                                                                    <i class="fa fa-lock"></i>
                                                                </span>
                                                            </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label for="field-2" class="control-label">Re-Type New Password</label>
                                                            <div class="input-group form-group">
                                                                <input type="password" id="p2" name="p2" class="form-control" placeholder="Your Old Password"
                                                                required data-parsley-equalto="#password">
                                                                <span class="input-group-addon"  id="v2`">
                                                                    <i class="fa fa-keyboard-o"></i>
                                                                </span>
                                                            </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="submit-password" type="submit" class="btn btn-facebook waves-effect waves-light">Save </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                                </form>
<!-- Modal Pass -->

 <!-- Modal UpdUser -->
    <form action="<?php echo base_url($con). '/updAvaUser' ?>" method="post" data-parsley-validate novalidate enctype="multipart/form-data">
    <div id="details-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Change Avatar</h4>
                                                    <input type="hidden" name="id_user" value="<?=$get->id_user ?>">
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">Current Avatar</label>
                                                                <div class="thumbnail">
                                                                    <img width="200" src="<?= base_url('images/_advisors/'.$get->user_avatar)?>" class="img-rounded img-responsive" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label class="control-label">New Avatar</label>
                                                                <div class="">
                                                                    <input type="file" name="photo" class="filestyle" data-buttonname="btn-primary" required>
                                                                     </div><hr>

                                                                     <div class="">
                                                                        <a onclick="del('<?=$get->user_avatar ?>')" data-dismiss="modal" style="color: red">Delete Current Avatar <i class="fa fa-trash"></i></a>
                                                                     </div><hr>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="submit-password" type="submit" class="btn btn-facebook waves-effect waves-light">Save </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                                </form>
<!-- Modal Add -->

        <script type="text/javascript">
            function change()
         {
            var x = document.getElementById('old').type;

            if (x == 'password')
            {
               document.getElementById('old').type = 'text';
               document.getElementById('see').innerHTML = '<span style="text-decoration:line-through"> Show </span>';
            }
            else
            {
               document.getElementById('old').type = 'password';
               document.getElementById('see').innerHTML = 'Show';
            }
         }
        </script>

       

 <script>
$(document).ready(function(){
 $('form').parsley();
});
</script>

<script>//Swal Delete Offense
 function del(ava) {
        var ava;
        if (ava == 'default.png') {
            swal('Ambigous','User already use default avatar', 'warning')
            return false;
        }else{
         swal({
        
  title: "Delete Avatar?",
  text: "Are you sure? Your will not be able to recover this avatar! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Delete!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url($con).'/delAvaUser/';?>";

});
}
     }
</script>


 <?php echo $this->session->flashdata('notify') ?>
</html>