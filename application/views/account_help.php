<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>PASIM | Help</title>

  <?php include '_source/landing_top.php'; ?>

</head>

<body class=" bg-dark">

<div class="body-inner">
	<!-- Content Start-->
<section>
	<form method="post" action="<?=base_url('Auth/trouble_account') ?>">
	<div class="container text-center">
		<div class="text-center">
			<img style="max-width: 150px;" class="logo-center" src="<?= base_url('assets/landing/images/lock.png')?>">
			<h3 style="color: #0191DD">Account Help</h3>
			<p style="color: lime;"><?= $this->session->flashdata('notify'); ?></p>
			<p style="color: red;"><?= $this->session->flashdata('notfound'); ?></p>
			<hr>
		<div class="container text-center col-md-4" style="top: 25px">
			<div class="form-group">
				<p>Enter your username or email and we'll send you a link to get back into your account.</p>
			</div>

			<div class="form-group">
				<input placeholder="Username / Email" type="text" name="contact" class="form-control">
			</div>

			<div class="form-group">
				<button class="btn btn-success" type="submit"> Get Help</button>

			</div>

			
		</div>

		</div>

	</div>

</form>
</section>
<div class="form-group text-center" >
				<a style="color: white" href="<?=base_url('login/') ?>">Back to Login</a>
			</div>

	<!-- Content End -->


</div><!-- End Inner Body -->
<?php include '_source/landing_bottom.php'; ?>

</body>


</html>