<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('condition') != 'Online') {
			$this->session->set_flashdata('notify', 'Login First');
			redirect(base_url('login'));
		}
		
		if ($this->session->userdata('role') != 'Advisors') {
			$this->session->set_flashdata('notify', 'Access Denied!');
			redirect(base_url('login'));
		}

		$this->load->model('Mc');
		$this->load->model('Ma');
	}
	
	public function index(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['recSupervised'] = $this->Mc->recSupervised()->result();
		$data['recWarned1'] = $this->Mc->recWarned1()->result();
		$data['recWarned2'] = $this->Mc->recWarned2()->result();
		$data['recWarned3'] = $this->Mc->recWarned3()->result();

		$this->load->view('Master/dashboard', $data);
	}
	
	function account(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$this->load->view('account', $data);
	}


	function students(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();

		$data['getStudent'] = $this->Ma->getStudent()->result();
		$data['getExpertise'] = $this->Ma->getExpertise()->result();

		$this->load->view('Master/students', $data);
	}

	function records(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();

		$data['getRecordList'] = $this->Mc->getRecordList()->result();

		$this->load->view('Master/record', $data);
	}

	//Catch Function

	function catchStudent($id_student){
		$data_session = array('id_student'  => $id_student);

		$this->session->set_userdata($data_session);
		redirect('Master/getStudent/');

	}

	function catchLetter($id_warnings){
		$data_session = array(
				'id_warnings'  => $id_warnings
			);

		$this->session->set_userdata($data_session);
		redirect('Master/warning_letter/');

	}

	function getStudent(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();
		$data['getOffense'] = $this->Ma->getOffense()->result();

		$data['getStudent'] = $this->Ma->getStudent()->result();
		$data['getCatchStudent'] = $this->Ma->getCatchStudent()->result();
		$data['getCSWarningLetter'] = $this->Ma->getCSWarningLetter()->result();
		$data['getExpertise'] = $this->Ma->getExpertise()->result();
		$id_student = $this->session->userdata('id_student');
		$where = array('id_student' => $id_student);
		$data['countRecord'] = $this->Mc->get('record',$where)->num_rows();
		$data['countAchievement'] = $this->Mc->get('achievement',$where)->num_rows();

		if ($this->session->userdata('id_student') == NULL) {
			$this->session->set_flashdata("notify", "<script>
    			window.onload=function(){
        		swal('Select Student','If you want to Read Student Details Click `More Details` on Student Lists ', 'info')
   				 }
 				</script>");
			redirect(base_url('Master/students/'));
		}

		$this->load->view('Master/getStudent', $data);
	}

	function expertise(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['getExpertise'] = $this->Ma->getExpertise()->result();

		$this->load->view('Master/expertise', $data);
	}

	function offense(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['getOffense'] = $this->Ma->getOffense()->result();

		$this->load->view('Master/offense', $data);
	}

	function charter(){
		$data['getStudent'] = $this->Ma->getStudent()->result();

		$this->load->view('charter.php', $data);
	}

	function report(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['getAllReport'] = $this->Mc->getAllReport()->result();
		$data['getAccReport'] = $this->Mc->getAccReport()->result();
		$data['getDecReport'] = $this->Mc->getDecReport()->result();
		$data['getWaitReport'] = $this->Mc->getWaitReport()->result();
		$this->load->view('Master/report', $data);
	}

	function accReport(){
		$id_report = $this->input->post('id_report');
		$cost = $this->input->post('cost');
		$where = array('id_report_violation' => $id_report);
        $report_vio = $this->Mc->get('report_violation',$where)->result();
        foreach ($report_vio as $rv) {}
        	$id_user = $this->session->userdata('id_user');
		$data = array(
			'id_student' => $rv->id_student,
			'id_offense' => $rv->id_offense,
			'id_user' => $id_user,
			'about_record' => $rv->about_record,
			'doc_img' => $rv->doc_img,
			'cost' => $cost
		);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Accepted','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");

		$this->autoStat($id_report);
		$this->autoReceiver($id_user);
		$this->Mc->add($data,'record');

		redirect(base_url('Master/report/'));

	} 

	private function autoStat($id_report){
		$where = array('id_report_violation' => $id_report);
		$data = array('stat' => 'Accepted');
		return $this->Mc->upd($where,$data,'report_violation');
	}

	private function autoReceiver($id_user){
		$where = array('id_report_violation' => $id_user);
		$data = array('id_receiver' => $id_user);
		return $this->Mc->upd($where,$data,'report_violation');
	}

	function decReport($id_report){
		$where = array('id_report_violation' => $id_report);
		$data = array('stat' => 'Rejected');
		$this->Mc->upd($where,$data,'report_violation');
		
		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Rejected','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");

		redirect(base_url('Master/report/'));
		
	}

	//Add Function

	function addExpertise(){
		$name_expertise = $this->input->post('name_expertise');
		$alias_expertise = $this->input->post('alias_expertise');
		$about_expertise = $this->input->post('about_expertise');

		$data = array(
			'name_expertise' => $name_expertise,
			'alias_expertise' => $alias_expertise,
			'about_expertise' => $about_expertise
			);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Saved','Your Data Has Been Successfully Saved', 'success')
    }
 </script>");

		$this->Mc->add($data,'expertise');
		redirect('Master/expertise/');

		
	}

	function addOffense(){
		$name_offense = $this->input->post('name_offense');
		$level_offense = $this->input->post('level_offense');
		$about_offense = $this->input->post('about_offense');

		$data = array(
			'name_offense' => $name_offense,
			'level_offense' => $level_offense,
			'about_offense' => $about_offense
			);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Saved','Your Data Has Been Successfully Saved', 'success')
    }
 </script>");

		$this->Mc->add($data,'offense');
		redirect('Master/offense/');

		
	}

	function addStudent(){
		$nis = $this->input->post('nis');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$id_expertise = $this->input->post('id_expertise');
		$gender = $this->input->post('gender');
		$start_period = $this->input->post('start_period');
		$grade = $this->input->post('grade');
		$student_email = $this->input->post('student_email');

		$data = array(
			'nis' => $nis,
			'name' => $name,
			'address' => $address,
			'id_expertise' => $id_expertise,
			'gender' => $gender,
			'point' => '0',
			'start_period' => $start_period,
			'grade' => $grade,
			'status' => 'Active',
			'student_stages' => 'Ordinary',
			'password' => 'none',
			'student_email' => $student_email
			);

		if (empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data['student_avatar'] = "default.png";
		}

		elseif (!empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data['student_avatar'] = $photo;
		}

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Added','Your Data Has Been Successfully Added', 'success')
   		 }
 		</script>");

		$this->Mc->add($data,'student');
		$this->add_student_token($nis);
		redirect('Master/students/');
	}

	private function add_student_token($nis){
		$rand = array(
			range(1, 9) ,
			range('a', 'z'),
	);
		$char = array();
		foreach ($rand as $r => $value) {
			foreach ($value as $r => $v) {
				$char[] = $v;
			}
		}

		$rand = null;
		for ($i=1; $i<=8 ; $i++) { 
			$rand .= $char[rand($i, count($char) - 1)];
		}

		$where = array('nis' => $nis);
		$catchNewStudent = $this->Mc->get('student',$where)->result();
		foreach ($catchNewStudent as $info) {}
		$primary = $info->id_student.$rand.'z';			

		$id_student = $info->id_student;
		$data = array(
			'id_student' => $info->id_student,
			'token' => $primary,
			'temp' => 'exit',
			'exp' => '0000-00-00' 
		);

		$this->auto_student_password($id_student,$primary);
		return $this->Mc->add($data,'student_token');

	}

	private function auto_student_password($id_student,$primary){
		$where = array('id_student' => $id_student);
		$options = ['cost' => 12];
		$encrypt = password_hash($primary, PASSWORD_BCRYPT, $options);
		$data = array('password' => $encrypt);
		
		return $this->Mc->upd($where,$data,'student');		
	}

	private function swal_new_student($id_student,$warned_type,$letter_reason){
          $config['mailtype'] = 'html';
          $config['protocol'] = 'smtp';
          $config['smtp_host'] = 'smtp.gmail.com';
          $config['smtp_user'] = 'rhnmhmd19@gmail.com';
          $config['smtp_pass'] = 'chewy123';
          $config['smtp_port'] = 465;
          $config['smtp_crypto'] = 'ssl';
          $config['charset'] = 'utf-8';
          $config['newline'] = "\r\n";
          $where = array('id_student' => $id_student);
          $student_info = $this->Mc->get('student',$where)->result();
          foreach ($student_info as $si) {}
          	$id_expertise = $si->id_expertise;
          	$grade = $si->grade;
          	$student_homeroom = $this->Ma->homeroomAvail($id_expertise,$grade)->result();
          foreach ($student_homeroom as $sh) {}
          	$where_id_user = array('id_user' => $sh->id_user);
          	$user_info = $this->Mc->get('user',$where_id_user)->result();
          foreach ($user_info as $ui) {}

          $mailto = $ui->email;
          $content = '<strong>Hello, '.$ui->s_name.'</strong><br><strong>Your Student has given Warning, </strong> <br>it`s the details you should know.<br><br> Name : '.$si->name.'<br> NIS :  '.$si->nis.'<br> Grade : '.$si->grade.'<br>Warning Type : <strong>'.$warned_type.'</strong>,<br>Advisors Reason : '.$letter_reason.'<br> You can check with your homeroom account, <a href="http://localhost/advisors/login"> <b>Login Page</b></a>';

          $this->load->library('email', $config);

          $this->email->from('no-reply@rakbar.com', 'Student Advisors');
          $this->email->to($mailto);
          $this->email->subject('Student Warning');
          $this->email->message($content);

          if($this->email->send()) {
               echo 'Mail Sended';
          }
          else {
               echo 'Sorry, The postman is sick';
               //echo '<br />';
               //echo $this->email->print_debugger();
          }
}

	function addRecord(){
		$id_student = $this->input->post('id_student');
		$id_offense = $this->input->post('id_offense');
		$id_user = $this->input->post('id_user');
		$about_record = $this->input->post('about_record');
		$d_record = $this->input->post('d_record');
		$cost = $this->input->post('cost');

		$date_source = explode('-', $d_record);
		$array = array($date_source[2], $date_source[1], $date_source[0]); 
		$date_record = implode('-', $array);

		$data = array(
			'id_student' => $id_student,
			'id_offense' => $id_offense,
			'id_user' => $id_user,
			'about_record' => $about_record,
			'd_record' => $date_record,
			'cost' => $cost
		);

		if (empty($_FILES['photo']['name'])) {
			$photo = $this->doc_upload();
			$data['doc_img'] = "default.png";
		}

		elseif (!empty($_FILES['photo']['name'])) {
			$photo = $this->doc_upload();
			$data['doc_img'] = $photo;
		}

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Case Recorded','Your Data Has Been Successfully Saved', 'success')
    	}
 		</script>");

		$this->Mc->add($data,'record');
		redirect('Master/getStudent/');
	}

	function addAchievement(){
		$id_student = $this->input->post('id_student');
		$id_user = $this->session->userdata('id_user');
		$achievement_title = $this->input->post('achievement_title');
		$date_achievement = $this->input->post('date_achievement');
		$about_achievement = $this->input->post('about_achievement');

		$date_source = explode('-', $date_achievement);
		$array = array($date_source[2], $date_source[1], $date_source[0]); 
		$achievement_date = implode('-', $array);

		$data = array(
			'id_student' => $id_student,
			'id_user' => $id_user,
			'achievement_title' => $achievement_title,
			'about_achievement' => $about_achievement,
			'achievement_date' => $achievement_date
		);

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Achievement Saved','Your Data Has Been Successfully Saved', 'success')
    	}
 		</script>");

		$this->Mc->add($data,'achievement');
		redirect('Master/getStudent/');

	}

	function addLetter(){
		$id_student = $this->session->userdata('id_student');
		$id_user = $this->session->userdata('id_user');
		$warned_type = $this->input->post('warned_type');
		$letter_title = $this->input->post('letter_title');
		$concerning_subject = $this->input->post('concerning_subject');
		$d_show = $this->input->post('d_show');
		$m_show = $this->input->post('m_show');
		$y_show = $this->input->post('y_show');
		$letter_reason = $this->input->post('letter_reason');

		$date_show = $d_show.', '.$m_show.' ' .$y_show;

		$data = array(
			'id_student' => $id_student,
			'id_user' => $id_user,
			'warned_type' => $warned_type,
			'letter_title' => $letter_title,
			'concerning_subject' => $concerning_subject,
			'date_show' =>$date_show,
			'letter_reason' => $letter_reason 
		);

		$this->updStages($warned_type);
		$this->alertHomeroom($id_student,$warned_type,$letter_reason);
		$this->session->set_flashdata("notify", "<script>
    						window.onload=function(){
    						    swal('Letter Created','Student Stages Automaticly Updated to ' + '$warned_type','success')
   						 }
 						</script>");

		$this->Mc->add($data,'warnings');
		redirect('Master/getStudent/');
	}

	private function alertHomeroom($id_student,$warned_type,$letter_reason){
          $config['mailtype'] = 'html';
          $config['protocol'] = 'smtp';
          $config['smtp_host'] = 'smtp.gmail.com';
          $config['smtp_user'] = 'rhnmhmd19@gmail.com';
          $config['smtp_pass'] = 'chewy123';
          $config['smtp_port'] = 465;
          $config['smtp_crypto'] = 'ssl';
          $config['charset'] = 'utf-8';
          $config['newline'] = "\r\n";
          $where = array('id_student' => $id_student);
          $student_info = $this->Mc->get('student',$where)->result();
          foreach ($student_info as $si) {}
          	$id_expertise = $si->id_expertise;
          	$grade = $si->grade;
          	$student_homeroom = $this->Ma->homeroomAvail($id_expertise,$grade)->result();
          foreach ($student_homeroom as $sh) {}
          	$where_id_user = array('id_user' => $sh->id_user);
          	$user_info = $this->Mc->get('user',$where_id_user)->result();
          foreach ($user_info as $ui) {}

          $mailto = $ui->email;
          $content = '<strong>Hello, '.$ui->s_name.'</strong><br><strong>Your Student has given Warning, </strong> <br>it`s the details you should know.<br><br> Name : '.$si->name.'<br> NIS :  '.$si->nis.'<br> Grade : '.$si->grade.'<br>Warning Type : <strong>'.$warned_type.'</strong>,<br>Advisors Reason : '.$letter_reason.'<br> You can check with your homeroom account, <a href="http://localhost/advisors/login"> <b>Login Page</b></a>';

          $this->load->library('email', $config);

          $this->email->from('no-reply@rakbar.com', 'Student Advisors');
          $this->email->to($mailto);
          $this->email->subject('Student Warning');
          $this->email->message($content);

          if($this->email->send()) {
               echo 'Mail Sended';
          }
          else {
               echo 'Sorry, The postman is sick';
               //echo '<br />';
               //echo $this->email->print_debugger();
          }
}

	function letter(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();

		$data['getAllLetter'] = $this->Ma->getAllLetter()->result();

		$this->load->view('Master/letter', $data);
	}

	//Del Function

	function delExpertise($id_expertise){
		$where = array('id_expertise'=>$id_expertise);

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Deleted','Your Data Has Been Successfully Deleted', 'success')
    	}
 		</script>");

		$this->Mc->del($where,'expertise');
		redirect('Master/expertise/');
	}

	function delOffense($id_offense){
		$where = array('id_offense'=>$id_offense);

		$this->Mc->del($where,'offense');

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Deleted','Your Data Has Been Successfully Deleted', 'success')
    	}
 		</script>");

		redirect('Master/offense/');
	}

	function delAvaStudent(){
		$getCatchStudent = $this->Ma->getCatchStudent()->result();
		foreach ($getCatchStudent as $s) {}
		$where = array('id_student'=>$s->id_student);
		$this->_do_delete($ava);
		$data = array('student_avatar' => 'default.png');

			$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Success','Avatar Deleted, now using default avatar', 'success')
    	}
 		</script>");	
			$this->Mc->upd($where,$data,'student');
		
			redirect('Master/getStudent/');
		
			
	}

	function delAvaUser(){
		$detailUser = $this->Mc->detailUser()->result();
		foreach ($detailUser as $u) {}
		$where = array('id_user' => $u->id_user);
		$this->_user_delete($ava);
		$data = array('user_avatar' => 'default.png');

			$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Success','Avatar Deleted, now using default avatar', 'success')
    	}
 		</script>");	
			$this->Mc->upd($where,$data,'user');
		
			redirect('Master/account/');
		
			
	}

	//Upd Function

	function updExpertise(){
		$id_expertise = $this->input->post('id_expertise');
		$name_expertise = $this->input->post('name_expertise');
		$alias_expertise = $this->input->post('alias_expertise');
		$about_expertise = $this->input->post('about_expertise');

		$data = array(
			'id_expertise' => $id_expertise,
			'name_expertise' => $name_expertise,
			'alias_expertise' => $alias_expertise,
			'about_expertise' => $about_expertise
			);
 
	$where = array(
		'id_expertise' => $id_expertise
	);
	
	$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");

	$this->Mc->upd($where,$data,'expertise');
			redirect(base_url("Master/expertise/"));
}

function updOffense(){
		$id_offense = $this->input->post('id_offense');
		$name_offense = $this->input->post('name_offense');
		$level_offense = $this->input->post('level_offense');
		$about_offense = $this->input->post('about_offense');

		$data = array(
			'id_offense' => $id_offense,
			'name_offense' => $name_offense,
			'level_offense' => $level_offense,
			'about_offense' => $about_offense
			);
 
	$where = array(
		'id_offense' => $id_offense
	);

	$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");
 
	$this->Mc->upd($where,$data,'offense');
			redirect(base_url("Master/offense/"));
}

function updStudent(){
		$id_student = $this->input->post('id_student');
		$name = $this->input->post('name');
		$nis = $this->input->post('nis');
		$address = $this->input->post('address');
		$id_expertise = $this->input->post('id_expertise');
		$start_period = $this->input->post('start_period');
		$grade = $this->input->post('grade');
		$gender = $this->input->post('gender');
		$student_stages = $this->input->post('student_stages');

		$data = array(
			'id_student' => $id_student,
			'name' => $name,
			'nis' => $nis,
			'address' => $address,
			'id_expertise' => $id_expertise,
			'grade' => $grade,
			'gender' => $gender,
			'start_period' => $start_period,
			'student_stages' => $student_stages
			);
 
	$where = array(
		'id_student' => $id_student
	);

	$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");
 
			$this->Mc->upd($where,$data,'student');
			redirect(base_url("Master/getStudent/"));
}

function updAvaStudent(){
	$getCatchStudent = $this->Ma->getCatchStudent()->result();
	foreach ($getCatchStudent as $s) {}
		$where = array(
		'id_student' => $s->id_student
		);

		if ($s->student_avatar != 'default.png') {
			$this->_do_delete($ava);
					if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data = array('student_avatar' => $photo);
		}

		}elseif ($s->student_avatar == 'default.png') {
				if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data = array('student_avatar' => $photo);
		}
		}
		//$data = array('student_avatar' => $photo);

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Updated','Avatar Has Been Successfully Changed', 'success')
   		 }
 		</script>");

		$data = array('student_avatar' => $photo);

		$this->Mc->upd($where,$data,'student');
		redirect(base_url("Master/getStudent/"));

}

function updLetter(){
	$getCatchLetter = $this->Ma->getCatchLetter()->result();
	foreach ($getCatchLetter as $l) {}
		$id_warnings = $l->id_warnings;
		$letter_title = $this->input->post('letter_title');
		$concerning_subject = $this->input->post('concerning_subject');
		$letter_reason = $this->input->post('letter_reason');
		$date_show = $this->input->post('date_show');

		$data = array(
			'letter_title' => $letter_title,
			'concerning_subject' => $concerning_subject,
			'letter_reason' => $letter_reason,
			'date_show' => $date_show
		);

		$where = array('id_warnings' => $id_warnings);

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Updated','Letter Has Been Successfully Updated', 'success')
   		 }
 		</script>");

 		$this->Mc->upd($where,$data,'warnings');
		redirect(base_url("Master/warning_letter/"));


}

/// FOR Account CONTROLLER

function updAvaUser(){
	$detailUser = $this->Mc->detailUser()->result();
	foreach ($detailUser as $u) {}
	$where = array('id_user' => $u->id_user);

		if ($u->user_avatar != 'default.png') {
			$this->_user_delete($ava);
					if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_user_upload();
			$data = array('user_avatar' => $photo);
		}

		}elseif ($u->user_avatar == 'default.png') {
				if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_user_upload();
			$data = array('user_avatar' => $photo);
		}
		}
		//$data = array('student_avatar' => $photo);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Avatar Has Been Successfully Changed', 'success')
    }
 </script>");

		$data = array('user_avatar' => $photo);

		$this->Mc->upd($where,$data,'user');
		redirect(base_url("Master/account/"));
		
}


function updPassword(){
		$id_user = $this->input->post('id_user');
		$password = $this->input->post('password');
		$old = $this->input->post('old');

		$get = array('id_user' => $id_user);

		$options = ['cost' => 12];
		$encrypt = password_hash($password, PASSWORD_BCRYPT, $options);
		$getUser = $this->Mc->get('user',$get)->result();

		foreach ($getUser as $info) {
			if (password_verify($old, $info->password)) {
				
				$data = array(
					'id_user' => $id_user,
					'password' => $encrypt
				);	

				$where = array(
				'id_user' => $id_user);

				$this->Mc->upd($where,$data,'user');
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Changed','Your Password Has Been Successfully Changed', 'success')
    }
 </script>");

				redirect(base_url('/Master/account/'));
			}else{
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Incorect','Sorry your Password not changed, Please check and Try again!', 'error')
    }
 </script>");
				redirect(base_url('/Master/account/'));
			}
		}

	}

/// FOR IMAGE CONTROLLER
	// print_r($this->upload->display_errors());
	//in case u need this, deactive return first!

private function _do_upload(){
		$config['upload_path'] = './images/_student';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	}


private function _do_delete($ava){
		$getCatchStudent = $this->Ma->getCatchStudent()->result();
		foreach ($getCatchStudent as $s) {}
		$where = array('id_student'=>$s->id_student);
		
			if ($s->student_avatar != 'default.png') {
				return unlink('./images/_student/'.$s->student_avatar);
				
		}else{
			//$data = array('student_avatar' => 'default.png');
			redirect('Master/getStudent/');
		}
}



private function doc_upload(){
		$config['upload_path'] = './images/_documentation';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	
}

private function _user_upload(){
		$config['upload_path'] = './images/_advisors';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	}

	private function _user_delete($ava){
	$detailUser = $this->Mc->detailUser()->result();
	foreach ($detailUser as $u) {}
	$where = array('id_user' => $u->id_user);
		
			if ($u->user_avatar != 'default.png') {
				return unlink('./images/_advisors/'.$u->user_avatar);
				
		}else{
			//$data = array('student_avatar' => 'default.png');
			redirect('Master/account/');
		}
}

//Extras

function warning_letter(){
	$data['getCatchStudent'] = $this->Ma->getCatchStudent()->result();
	$data['getMenu'] = $this->Mc->getMenu()->result();
	$data['detailUser'] = $this->Mc->detailUser()->result();
	$data['getCatchLetter'] = $this->Ma->getCatchLetter()->result();

	if ($this->session->userdata('id_warnings') == NULL) {
			$this->session->set_flashdata("notify", "<script>
    			window.onload=function(){
        		swal('Select Letter','If you want to Print Warning Letter Click `More Details` on Student Lists ', 'info')
   				 }
 				</script>");
			redirect(base_url('Master/students/'));
		}

	$this->load->view('extras/warning_letter', $data);
}

// PDF

function pdf(){
	$data['achie'] = $this->Ma->getCSAchievement()->result();
	$this->load->view('pdf_test.php', $data);
}

function pdf_test(){
	$this->load->library('dompdf_gen');

	$data['achie'] = $this->Ma->getCSAchievement()->result();

	$this->load->view('pdf_test.php', $data);

	$paper_size = 'A4';
	$orientation = 'potrait';
	$html = $this->output->get_output();
	$this->dompdf->set_paper($paper_size, $orientation);

	$this->dompdf->load_html($html);
	$this->dompdf->render();
	$this->dompdf->stream("achie_pdf.pdf", array('Attachment' => FALSE));

}

// End PDF

//Private Dual Function

private function updStages($warned_type){
	$id_student = $this->session->userdata('id_student');
	$where  = array('id_student' => $id_student);
	$data = array('student_stages' => $warned_type);

	return $this->Mc->upd($where,$data,'student');
}

}//End
