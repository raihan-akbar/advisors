<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('Mb');
	}
	
	public function index()
	{
		$data['topThree'] = $this->Mb->topThree()->result();
		$this->load->view('home',$data);
	}

	function terms()
	{
		$this->load->view('terms');
	}

	function help()
	{
		$this->load->view('help');
	}
	
	function login(){
		$this->load->view('login');
	}

	function account_help(){
		$this->load->view('account_help');
	}

	function nf(){
		$this->load->view('nf');
	}

	
}//End
