<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeroom extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('condition') != 'Online') {
			$this->session->set_flashdata('notify', 'Login First');
			redirect(base_url('login'));
		}
		
		if ($this->session->userdata('role') != 'Homeroom') {
			$this->session->set_flashdata('notify', 'Access Denied!');
			redirect(base_url('login'));
		}

		$this->load->model('Mc');
		$this->load->model('Ma');
		$this->load->model('Mh');
	}

	public function index(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$this->load->view('Homeroom/dashboard', $data);
	}

	function student(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['notAllStudent'] = $this->Mh->notAllStudent()->result();
		$data['bundleInfo'] = $this->Mh->bundleInfo()->result();

		$this->load->view('Homeroom/student', $data);
	}

		function catchStudent($id_student){
		$data_session = array(
				'id_student'  => $id_student
			);

		$this->session->set_userdata($data_session);
		redirect('Homeroom/getStudent/');

	}

	function catchLetter($id_warnings){
		$data_session = array(
				'id_warnings'  => $id_warnings
			);

		$this->session->set_userdata($data_session);
		redirect('Homeroom/warning_letter/');

	}

	function warning_letter(){
	$data['getCatchStudent'] = $this->Ma->getCatchStudent()->result();
	$data['getMenu'] = $this->Mc->getMenu()->result();
	$data['detailUser'] = $this->Mc->detailUser()->result();
	$data['getCatchLetter'] = $this->Ma->getCatchLetter()->result();

	if ($this->session->userdata('id_warnings') == NULL) {
			$this->session->set_flashdata("notify", "<script>
    			window.onload=function(){
        		swal('Select Letter','If you want to Print Warning Letter Click `More Details` on Student Lists ', 'info')
   				 }
 				</script>");
			redirect(base_url('Master/students/'));
		}

	$this->load->view('extras/warning_letter', $data);
}

	function getStudent(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();
		$data['getOffense'] = $this->Ma->getOffense()->result();

		$data['getStudent'] = $this->Ma->getStudent()->result();
		$data['getCatchStudent'] = $this->Ma->getCatchStudent()->result();
		$data['getCSWarningLetter'] = $this->Ma->getCSWarningLetter()->result();
		$data['getExpertise'] = $this->Ma->getExpertise()->result();
		$id_student = $this->session->userdata('id_student');
		$where = array('id_student' => $id_student);
		$data['countRecord'] = $this->Mc->get('record',$where)->num_rows();
		$data['countAchievement'] = $this->Mc->get('achievement',$where)->num_rows();

		if ($this->session->userdata('id_student') == NULL) {
			$this->session->set_flashdata("notify", "<script>
    			window.onload=function(){
        		swal('Select Student','If you want to Read Student Details Click `More Details` on Student Lists ', 'info')
   				 }
 				</script>");
			redirect(base_url('Homeroom/students/'));
		}

		$this->load->view('Homeroom/getStudent', $data);
	}

}

/* End of file Homeroom.php */
/* Location: ./application/controllers/Homeroom.php */