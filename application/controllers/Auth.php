<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Mc');
		$this->load->model('Ms');
	}//()
	
	function authentication(){//()
	date_default_timezone_set("Asia/Jakarta");

$b = time();
$hour = date("G",$b);

if ($hour>=0 && $hour<=11)
{
$greet = "Good Morning";
}
elseif ($hour >=12 && $hour<=14)
{
$greet = "Good Afternoon ";
}
elseif ($hour >=15 && $hour<=17)
{
$greet = "Good Afternoon ";
}
elseif ($hour >=17 && $hour<=18)
{
$greet = "Good Evening ";
}
elseif ($hour >=19 && $hour<=23)
{
$greet = "Good Night ";
}

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$where = array('username' => $username);
		$user = $this->Mc->get('user',$where)->num_rows();
		$info = $this->Mc->get('user',$where)->result();
		$where_nis = array('nis' => $username);
		$check_nis = $this->Mc->get('student',$where_nis)->num_rows();
		$student_info = $this->Mc->get('student',$where_nis)->result();

		if ($user == '1') {//1 Check Username
			foreach ($info as $i) {//a
				if (password_verify($password, $i->password)) {//2 Check Password
					if ($i->id_role == '1') {//3 Check Role
						
						$data_session = array(
							'username' => $username,
							'id_user' => $i->id_user,
							'condition' => 'Online',
							'con' => 'Master',
							'role' =>'Advisors'
						);

						$this->session->set_flashdata("notify", "<script>
    						window.onload=function(){
    						    swal('$greet','Hello,' \n+' $i->s_name','success')
   						 }
 						</script>");

						$this->session->set_userdata($data_session);
						redirect(base_url('Master/', $data));
						
					}//3

					else if ($i->id_role == '2') {//4 Check Role

						$data_session = array(
							'username' => $username,
							'id_user' => $i->id_user,
							'condition' => 'Online',
							'role' =>'Teacher',
							'con' => 'Teacher'
						);

						$this->session->set_userdata($data_session);

						redirect(base_url('Teacher/', $data));
						
					}//4 Check Role

					else if ($i->id_role == '3') {//5 Check Role

						$data_session = array(
							'username' => $username,
							'id_user' => $i->id_user,
							'condition' => 'Online',
							'role' =>'Student'
						);

						$this->session->set_userdata($data_session);

						redirect(base_url('Student/', $data));
						
					}//5 Check Role

					else if ($i->id_role == "4") {//6 Check Role

						$data_session = array(
							'username' => $username,
							'id_user' => $i->id_user,
							'condition' => 'Online',
							'con' => 'Admin',
							'role' =>'Admin'
						);

						$this->session->set_flashdata("notify", "<script>
    						window.onload=function(){
    						    swal('$greet','Hello,' \n+' $i->s_name','success')
   						 }
 						</script>");

						$this->session->set_userdata($data_session);

						redirect(base_url('Admin/', $data));

					}//6 Check Role
					else if ($i->id_role == "5") {//7 Check Role
						$where_id_user = array('id_user' => $i->id_user);
						$info_homeroom = $this->Mc->get('homeroom',$where_id_user)->result();
						foreach ($info_homeroom as $h) {}
						$data_session = array(
							'username' => $username,
							'id_user' => $i->id_user,
							'condition' => 'Online',
							'con' => 'Homeroom',
							'role' =>'Homeroom',
							'id_homeroom' => $h->id_homeroom,
							'grade' => $h->grade,
							'id_expertise' => $h->id_expertise
						);

						$this->session->set_flashdata("notify", "<script>
    						window.onload=function(){
    						swal('$greet','Hello,' \n+' $i->s_name','success')
   						 }
 						</script>");

						$this->session->set_userdata($data_session);

						redirect(base_url('Homeroom/', $data));

					}//7 Check Role

				}//2

				else{//Wrong Password
					$this->session->set_flashdata('notify', 'Incorrect Password');
					redirect(base_url('login'));
				}//Wrong Password

			}//a


		}//1

		else if ($check_nis == '1') {
			foreach ($student_info as $si) {}
				if (password_verify($password, $si->password)) {
						$data_session = array(
							'student_id' => $si->id_student,
							'condition' => 'Online',
							'con' => 'Student',
							'role' =>'Student',
							'grade' => $si->grade,
							'username' => $si->nis,
							'id_expertise' => $si->id_expertise
						);

					$this->session->set_flashdata("notify", "<script>
    						window.onload=function(){
    						swal('$greet','Hello,' \n+' $si->name','success')
   						 }
 						</script>");

					$this->session->set_userdata($data_session);
					redirect(base_url('Student/',$data));
				}
				else{
					$this->session->set_flashdata('notify', 'Incorrect Password');
					redirect(base_url('login'));
				}
		}

		else{//Wrong Password
			 $this->session->set_flashdata('notify', 'Username or Password Incorrect');
					redirect(base_url('login'));
		}//Wrong Username And Password

	}//()

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

	function trouble_account(){
		$contact = $this->input->post('contact');

		$check_username = array('username' => $contact);
		$check_email = array('email' => $contact);

		$username = $this->Mc->get('user',$check_username)->num_rows();
		$email = $this->Mc->get('user',$check_email)->num_rows();

		if ($username == '1') {
			$info = $this->Mc->get('user',$check_username)->result();
			foreach ($info as $i) {}
				$id = $i->id_user;
			$this->token_temp($id);
			$this->postman($id);
			$this->session->set_flashdata('notify', 'Success! Please check your Email for a link to reset your password.');
			redirect(base_url('Main/account_help'));
			
		}else if($email == '1'){
			$info = $this->Mc->get('user',$check_email)->result();
			foreach ($info as $i) {}
				$id = $i->id_user;
			$this->token_temp($id);
			$this->postman($id);
			$this->session->set_flashdata('notify', 'Success! Please check your Email for a link to reset your password.');
			redirect(base_url('Main/account_help'));
		}

		else{$this->session->set_flashdata('notfound', 'Sorry we don`t know that Account, check again please.');redirect(base_url('Main/account_help'));}

	}

	private function token_temp($id){
	$where = array('id_user' => $id);
	$user_info = $this->Mc->get('user',$where)->result();

	$rand = array(
			range(1, 9) ,
			range('a', 'y'),
	);
		$char = array();
		foreach ($rand as $r => $value) {
			foreach ($value as $r => $v) {
				$char[] = $v;
			}
		}

		$rand = null;
		for ($i=1; $i<=16 ; $i++) { 
			$rand .= $char[rand($i, count($char) - 1)];
		}

		$temp = $rand;
		$data = array('temp' => $temp);
		$this->adjust($id);
		return $this->Mc->upd($where,$data,'user_token');
	}

	private function adjust($id){
	date_default_timezone_set("Asia/Jakarta");
	$ymd = date('Y-m-d');
	$period = strtotime('+2 days', strtotime($ymd));
	$expired = date('Y-m-d', $period);
	$where = array('id_user' => $id);
	$data = array('exp' => $expired);
	return $this->Mc->upd($where,$data,'user_token');
}

	private function postman($id){
          $config['mailtype'] = 'html';
          $config['protocol'] = 'smtp';
          $config['smtp_host'] = 'smtp.gmail.com';
          $config['smtp_user'] = 'rhnmhmd19@gmail.com';
          $config['smtp_pass'] = 'chewy123';
          $config['smtp_port'] = 465;
          $config['smtp_crypto'] = 'ssl';
          $config['charset'] = 'utf-8';
          $config['newline'] = "\r\n";
          $where = array('id_user' => $id);
          $user_info = $this->Mc->get('user',$where)->result();
          $user_token = $this->Mc->get('user_token',$where)->result();
          foreach ($user_info as $i) {}
          foreach ($user_token as $t) {}

          $mailto = $i->email;
          $content = '<strong>Hello, '.$i->s_name.'</strong><br>This link is only valid for 1-2 days, the page will be destroyed automatically for your Security Reason. <br> <a target="_blank" href="http://localhost/advisors/rescuer/password/'.$t->token.$t->temp.' "> <strong>Click Here for Reset Your Password </strong><a/><br>.';

          $this->load->library('email', $config);

          $this->email->from('no-reply@rakbar.com', 'Student Advisors');
          $this->email->to($mailto);
          $this->email->subject('Reset Password');
          $this->email->message($content);

          if($this->email->send()) {
               echo 'Link Sended';
          }
          else {
               echo 'Sorry, Postman is not Working';
               //echo '<br />';
               //echo $this->email->print_debugger();
          }
}	

}//End
