<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('condition') != 'Online') {
			$this->session->set_flashdata('notify', 'Login First');
			redirect(base_url('login'));
		}
		
		if ($this->session->userdata('role') != 'Teacher') {
			$this->session->set_flashdata('notify', 'Access Denied!');
			redirect(base_url('login'));
		}

		$this->load->model('Mc');
		$this->load->model('Ma');
	}

	public function index(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$this->load->view('Teacher/dashboard',$data);
	}

	function student(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['getStudent'] = $this->Ma->getStudent()->result();

		$this->load->view('Teacher/passing', $data);
	}

	function catchStudent($id_student){
		$data_session = array('id_student'  => $id_student);

		$this->session->set_userdata($data_session);
		redirect('Teacher/getStudent/');

	}

	function getStudent(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['notificationList'] = $this->Mc->notificationList()->result();
		$data['getOffense'] = $this->Ma->getOffense()->result();

		$data['getStudent'] = $this->Ma->getStudent()->result();
		$data['getCatchStudent'] = $this->Ma->getCatchStudent()->result();
		$data['getCSWarningLetter'] = $this->Ma->getCSWarningLetter()->result();
		$data['getExpertise'] = $this->Ma->getExpertise()->result();
		$id_student = $this->session->userdata('id_student');
		$where = array('id_student' => $id_student);
		$data['countRecord'] = $this->Mc->get('record',$where)->num_rows();
		$data['countAchievement'] = $this->Mc->get('achievement',$where)->num_rows();

		if ($this->session->userdata('id_student') == NULL) {
			$this->session->set_flashdata("notify", "<script>
    			window.onload=function(){
        		swal('Select Student','If you want to Read Student Details Click `More Details` on Student Lists ', 'info')
   				 }
 				</script>");
			redirect(base_url('Teacher/student/'));
		}

		$this->load->view('Teacher/getStudent', $data);
	}

function addReport(){
		$id_student = $this->input->post('id_student');
		$id_offense = $this->input->post('id_offense');
		$id_reporter = $this->input->post('id_user');
		$about_record = $this->input->post('about_record');
		$d_record = $this->input->post('d_record');

		$date_source = explode('-', $d_record);
		$array = array($date_source[2], $date_source[1], $date_source[0]); 
		$date_record = implode('-', $array);

		$data = array(
			'id_student' => $id_student,
			'id_offense' => $id_offense,
			'id_reporter' => $id_reporter,
			'about_record' => $about_record,
			'd_record' => $date_record,
			'id_receiver' => '0',
			'stat' => 'Pending'

		);

		if (empty($_FILES['photo']['name'])) {
			$photo = $this->doc_upload();
			$data['doc_img'] = "default.png";
		}

		elseif (!empty($_FILES['photo']['name'])) {
			$photo = $this->doc_upload();
			$data['doc_img'] = $photo;
		}

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Case Recorded','Your Data Has Been Successfully Saved', 'success')
    	}
 		</script>");

		$this->Mc->add($data,'report_violation');
		redirect('Teacher/getStudent/');
	}

	private function doc_upload(){
		$config['upload_path'] = './images/_documentation';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	
}

}

/* End of file Teacher.php */
/* Location: ./application/controllers/Teacher.php */