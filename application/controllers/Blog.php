<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('Mc');
		$this->load->model('Ma');
		$this->load->model('Mb');
	}

	function index(){
		$data['getAllBlog'] = $this->Mb->getAllBlog()->result();
		$this->load->view('Blog/index', $data);
	}

	function article($blog_url){
		$where = array('blog_url'=>$blog_url);
		$data['getTouchBlog'] = $this->Mc->get('blog',$where)->result();

		$this->load->view('Blog/article', $data);
	}

}

/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */