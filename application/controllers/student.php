<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('condition') != 'Online') {
			$this->session->set_flashdata('notify', 'Login First');
			redirect(base_url('login'));
		}
		
		if ($this->session->userdata('role') != 'Student') {
			$this->session->set_flashdata('notify', 'Access Denied!');
			redirect(base_url('login'));
		}

		$this->load->model('Mc');
		$this->load->model('Ma');
		$this->load->model('Ms');
	}

	function index(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['studentBundle'] = $this->Ms->studentBundle()->result();

		$this->load->view('Student/Dashboard', $data);
	
	}

	function profile(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['studentBundle'] = $this->Ms->studentBundle()->result();

		$this->load->view('Student/profile', $data);

	}

	function updPassword(){
		$id_student = $this->input->post('id_student');
		$password = $this->input->post('password');
		$old = $this->input->post('old');

		$get = array('id_student' => $id_student);

		$options = ['cost' => 12];
		$encrypt = password_hash($password, PASSWORD_BCRYPT, $options);
		$getStudent = $this->Mc->get('student',$get)->result();

		foreach ($getStudent as $info) {
			if (password_verify($old, $info->password)) {
				
				$data = array(
					'id_student' => $id_student,
					'password' => $encrypt
				);	

				$where = array(
				'id_student' => $id_student);

				$this->Mc->upd($where,$data,'student');
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Changed','Your Password Has Been Successfully Changed', 'success')
    }
 </script>");

				redirect(base_url('/Student/profile/'));
			}else{
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Incorect','Sorry your Password not changed, Please check and Try again!', 'error')
    }
 </script>");
				redirect(base_url('/Student/Profile/'));
			}
		}

	}

	function teacher_chat(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['studentBundle'] = $this->Ms->studentBundle()->result();
		$data['getAllTeacher'] = $this->Ms->getAllTeacher()->result();

		$this->load->view('student/teacher_chat', $data);
	}

		function catchTeacherChat($id_user){
		$data_session = array('id_user_chat' => $id_user);

		$this->session->set_userdata($data_session);
		redirect('Student/stou/');

	}

	function stou(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['studentBundle'] = $this->Ms->studentBundle()->result();
		$data['getAllTeacher'] = $this->Ms->getAllTeacher()->result();

		$data['getCatchTeacherChat'] = $this->Ms->getCatchTeacherChat()->result();
		
		$this->load->view('Student/stou', $data);
	}


}

/* End of file student.php */
/* Location: ./application/controllers/student.php */