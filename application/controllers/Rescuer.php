<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rescuer extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('Mc');
		$this->load->model('Ma');
	}

	function password($token){
		//z Checker
			if (preg_match('/z/', $token)) {
				$match = 'clear';
			}else{redirect(base_url('broken_z'));}
		//Token and Temp Splitter
			$token_source = explode('z', $token);
			$xtemp = array($token_source[1]); 
			$ztemp = implode('', $xtemp);
			$count = strlen($ztemp);
			$perm = substr($token,0,-16);
			//$temp = substr($token, -16);
			if ($count != 16) {
				redirect(base_url('broken_c'));
			}
		//Token Checker
		$url_token = array('token' => $perm);
		$check_token = $this->Mc->get('user_token',$url_token)->num_rows();
		if ($check_token != 1) {
			redirect(base_url('broken_token'));
		}
		$url_temp = array('temp' => $ztemp);
		$check_temp = $this->Mc->get('user_token',$url_temp)->num_rows();
		if ($check_temp != 1) {		
			redirect(base_url('broken_temp'));
		}
		//Get Token
		$where_token = array('token' => $perm);
        $user_token = $this->Mc->get('user_token',$where_token)->result();
         foreach ($user_token as $t) {}
        $id_user = $t->id_user;
    	$where_id = array('id_user' => $id_user);
        $user_info = $this->Mc->get('user',$where_id)->result();       
         foreach ($user_info as $i) {}
		$data = array(
			'username' => $i->username,
			'id_user' => $i->id_user,
			'token' => $t->token,
			'temp' => $t->temp
		);
		//Expired Reader
		$ymd = date('Y-m-d');
		$expired = $t->exp;
		if ($ymd >= $expired) {
		redirect(base_url('broken_e'));
		}

	$this->load->view('Rescuer/r_pass', $data);
	

	}

	function rescue_password(){
		$token = $this->input->post('random');
		$id_user = $this->input->post('number');
		$newpassword = $this->input->post('newpassword');
		$repassword = $this->input->post('repassword');

		if ($repassword != $newpassword) {
			$this->session->set_flashdata('notify', 'Your New-Password not Same, Check Again');
			redirect(base_url('rescuer/password/'.$token));
			base_url($con). '/updAvaUser';
		}

		$options = ['cost' => 12];
		$encrypt = password_hash($newpassword, PASSWORD_BCRYPT, $options);

		$data = array('password' => $encrypt);
		$where = array('id_user' => $id_user);
		$this->Mc->upd($where,$data,'user');
		$this->session->set_flashdata('success', 'Your Password Has Been Successfully Changed');
		redirect(base_url('rescuer/password/'.$token));

	}


}/* End of file Rescuer.php */
/* Location: ./application/controllers/Rescuer.php */