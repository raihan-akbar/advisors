<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('condition') != 'Online') {
			$this->session->set_flashdata('notify', 'Login First');
			redirect(base_url('login'));
		}
		
		if ($this->session->userdata('role') != 'Admin') {
			$this->session->set_flashdata('notify', 'Access Denied!');
			redirect(base_url('login'));
		}

		$this->load->model('Mc');
		$this->load->model('Ma');
	}

	public function index(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$this->load->view('Admin/dashboard', $data);
	}

	function account(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$this->load->view('account', $data);
	}

	function point_limit(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();
		$data['allPointLimit'] = $this->Mc->allPointLimit()->result();

		$this->load->view('Admin/point_limit', $data);
	}

	function updPointLimit(){
		$id_extras = $this->input->post('id_extras');
		$point = $this->input->post('point');

		if ($point <= 0) {
			$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Oopss','Don`t Type Min (-) Because that is System Rules', 'warning')
    }
 </script>");

				redirect(base_url('Admin/point_limit/'));
		}

		$data = array('val_num' => '-'.$point);

		$where = array('id_extras' => $id_extras);

		$this->Mc->upd($where,$data,'extras');
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Updated','Point Has Been Successfully Changed', 'success')
    }
 </script>");

				redirect(base_url('Admin/point_limit/'));
	}

	function updPassword(){
		$id_user = $this->input->post('id_user');
		$password = $this->input->post('password');
		$old = $this->input->post('old');

		$get = array('id_user' => $id_user);

		$options = ['cost' => 12];
		$encrypt = password_hash($password, PASSWORD_BCRYPT, $options);
		$getUser = $this->Mc->get('user',$get)->result();

		foreach ($getUser as $info) {
			if (password_verify($old, $info->password)) {
				
				$data = array(
					'id_user' => $id_user,
					'password' => $encrypt
				);	

				$where = array('id_user' => $id_user);

				$this->Mc->upd($where,$data,'user');
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Changed','Your Password Has Been Successfully Changed', 'success')
    }
 </script>");

				redirect(base_url('/Admin/account/'));
			}else{
				$this->session->set_flashdata("notify", "<script>
    					window.onload=function(){
       					 swal('Password Incorect','Sorry your Password not changed, Please check and Try again!', 'error')
    }
 </script>");
				redirect(base_url('/Admin/account/'));
			}
		}

	}

	function users(){
		$data['getMenu'] = $this->Mc->getMenu()->result();
		$data['detailUser'] = $this->Mc->detailUser()->result();

		$data['getUserList'] = $this->Mc->getUserList()->result();
		$data['getRoleList'] = $this->Mc->getRoleList()->result();

		$this->load->view('Admin/users', $data);
	}

	function homeroom(){
	$data['getMenu'] = $this->Mc->getMenu()->result();
	$data['detailUser'] = $this->Mc->detailUser()->result();

	$data['getExpertise'] = $this->Ma->getExpertise()->result();
	$data['getAllTeacher'] = $this->Ma->getAllTeacher()->result();
	$data['getAllHomeroom'] = $this->Ma->getAllHomeroom()->result();

	$this->load->view('Admin/homeroom', $data);
	}

	function updUser(){
		$id_user = $this->input->post('id_user');
		$username = $this->input->post('username');
		$s_name = $this->input->post('s_name');
		$email = $this->input->post('email');
		$id_role = $this->input->post('id_role');

		$u_row = $this->Ma->unameChecker($username,$id_user)->num_rows();
		$e_row = $this->Ma->mailChecker($email,$id_user)->num_rows();

		if ($u_row == 1) {
			$this->session->set_flashdata("notify","
						<script>
    					window.onload=function(){
       					 swal('Sorry','Username Has been Registered by Another User', 'error')
    					}
 						</script>");
			redirect(base_url('Admin/users/'));
		}
		if ($e_row == 1) {
			$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Sorry','Email Has been Registered by Another User', 'error')
    					}
 						</script>");
			redirect(base_url('Admin/users/'));
		}
		else{
			$data = array(
				'username' => $username,
				's_name' =>  $s_name,
				'email' => $email,
				'id_role' => $id_role
			);
			
			$where = array('id_user' => $id_user);
			$this->Mc->upd($where,$data,'user');

			$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Success','User Data Has been Updated', 'success')
    					}
 						</script>");

			redirect(base_url('Admin/users/'));

		}

	}

	function addUser(){
		$username = $this->input->post('username');
		$s_name = $this->input->post('s_name');
		$email = $this->input->post('email');
		$id_role =  $this->input->post('id_role');
		$password = $this->input->post('password');

		$options = ['cost' => 12];
		$encrypt = password_hash($password, PASSWORD_BCRYPT, $options);

		$u_check = array('username' => $username);
		$e_check = array('email' => $email);

		$u_row = $this->Mc->get('user',$u_check)->num_rows();
		$e_row = $this->Mc->get('user',$e_check)->num_rows();

		if ($u_row == 1) {
			$this->session->set_flashdata("notify","
						<script>
    					window.onload=function(){
       					 swal('Sorry','Username Has been Registered by Another User', 'error')
    					}
 						</script>");
			redirect(base_url('Admin/users/'));
		}
		if ($e_row == 1) {
			$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Sorry','Email Has been Registered by Another User', 'error')
    					}
 						</script>");
			redirect(base_url('Admin/users/'));
		}

		$data = array(
			'username' => $username,
			's_name' => $s_name,
			'email' => $email,
			'id_role' => $id_role,
			'password' => $encrypt
		);

		if (empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data['user_avatar'] = "default.png";
		}

		else if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_do_upload();
			$data['user_avatar'] = $photo;
		}


		$this->session->set_flashdata("notify", "
			<script>
    			window.onload=function(){
       			swal('Added','Your Data Has Been Successfully Added', 'success')
    			}
 			</script>");

		$this->Mc->add($data,'user');
		$this->add_token($username);
		redirect(base_url('Admin/users/'));
		
	}

	private function add_token($username){
		$rand = array(
			range(1, 9) ,
			range('a', 'y'),
	);
		$char = array();
		foreach ($rand as $r => $value) {
			foreach ($value as $r => $v) {
				$char[] = $v;
			}
		}

		$rand = null;
		for ($i=1; $i<=6 ; $i++) { 
			$rand .= $char[rand($i, count($char) - 1)];
		}

		$where = array('username' => $username);
		$catchNewUser = $this->Mc->get('user',$where)->result();
		foreach ($catchNewUser as $info) {}
		$primary = $info->id_user.$rand.'z';			

		$data = array(
			'id_user' => $info->id_user,
			'token' => $primary,
			'temp' => 'exit',
			'exp' => '0000-00-00' 
		);

		return $this->Mc->add($data,'user_token');

	}

	function addHomeroom(){
		$id_user = $this->input->post('id_user');
		$id_expertise = $this->input->post('id_expertise');
		$grade = $this->input->post('grade');

		$where_id_user = array('id_user' => $id_user);
		$check_id_user = $this->Mc->get('homeroom',$where_id_user)->num_rows();
		$homeroomAvail = $this->Ma->homeroomAvail($id_expertise,$grade)->num_rows();

		if ($check_id_user == 1) {
			$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Ambigous','User Already as Homeroom', 'error')
    					}
 				</script>");
			redirect(base_url('Admin/homeroom'));
		}

		if ($homeroomAvail == 1) {
			$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Ambigous','Double Homeroom teacher are not permitted', 'error')
    					}
 				</script>");
			redirect(base_url('Admin/homeroom'));
		}
		

		$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Added','User Automatically set to Homeroom', 'success')
    					}
 				</script>");

		$data = array(
			'id_user' => $id_user,
			'id_expertise' => $id_expertise,
			'grade' => $grade
		);
		$this->autoHomeroom($id_user);
		$this->Mc->add($data,'homeroom');
		redirect(base_url('Admin/homeroom'));
	}

	function updHomeroom(){
		$id_user = $this->input->post('id_user');//New Homeroom id_user
		$current_homeroom = $this->input->post('current_homeroom');
		$id_homeroom = $this->input->post('id_homeroom');

		$where = array('id_user' => $current_homeroom);
		$data = array('id_role' => '2');

		$this->session->set_flashdata("notify","
				<script>
    					window.onload=function(){
       					 swal('Changed','Homeroom Teacher Changed', 'success')
    					}
 				</script>");

		$this->autoHomeroom($id_user);
		$this->swapRole($id_homeroom,$id_user);
		$this->Mc->upd($where,$data,'user');
		redirect(base_url('Admin/homeroom'));
	}

	private function autoHomeroom($id_user){
		$where = array('id_user' => $id_user);
		$data = array('id_role' => '5');
		return $this->Mc->upd($where,$data,'user');
	}

	private function swapRole($id_homeroom,$id_user){
		$where = array('id_homeroom' => $id_homeroom);
		$data = array('id_user' => $id_user);
		return $this->Mc->upd($where,$data,'homeroom');
	}


	
	function delUser($id_user){
		$where = array('id_user'=>$id_user);
		$getUser = $this->Mc->get('user',$where)->result();
		foreach ($getUser as $s) {}

			if ($s->user_avatar != 'default.png') {
			unlink('./images/_advisors/'.$s->user_avatar);
			}
			
		$this->Mc->del($where,'user');

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Deleted','Your Data Has Been Successfully Deleted', 'success')
    	}
 		</script>");
		$this->delToken($id_user);
		redirect(base_url('Admin/users/'));
	}
	private function delToken($id_user){
	$where = array('id_user'=>$id_user);
	return $this->Mc->del($where,'user_token');		
	}

	function addpost(){
	$data['getMenu'] = $this->Mc->getMenu()->result();
	$data['detailUser'] = $this->Mc->detailUser()->result();

	$data['getExpertise'] = $this->Ma->getExpertise()->result();
	$data['getAllTeacher'] = $this->Ma->getAllTeacher()->result();
	$data['getAllHomeroom'] = $this->Ma->getAllHomeroom()->result();

	$this->load->view('Admin/addpost', $data);
	}

	// Blog Section
	function posting(){
		$blog_title = $this->input->post('blog_title');
		$blog_content = $this->input->post('blog_content');
		$blog_type = $this->input->post('blog_type');

		$blog_date = date('y-m-d');
		$blog_author = $this->session->userdata('username');

		$rand = array(
			range(1, 9) ,
			range('a', 'x'),
	);
		$char = array();
		foreach ($rand as $r => $value) {
			foreach ($value as $r => $v) {
				$char[] = $v;
			}
		}

		$rand = null;
		for ($i=1; $i<=6 ; $i++) { 
			$rand .= $char[rand($i, count($char) - 1)];
		}

		$blog_url = $blog_type.'y'.$rand.'z';		

		$data = array(
			'blog_title' => $blog_title,
			'blog_content' => $blog_content,
			'blog_type' => $blog_type,
			'blog_date' => $blog_date,
			'blog_author' => $blog_author,
			'blog_url' => $blog_url
		 );

		if (empty($_FILES['photo']['name'])) {
			$photo = $this->_blog_upload();
			$data['blog_img'] = "default.png";
		}

		else if (!empty($_FILES['photo']['name'])) {
			$photo = $this->_blog_upload();
			$data['blog_img'] = $photo;
		}

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Uploaded','Your Blog Has Been Successfully Posted', 'success')
    	}
 		</script>");
		$this->Mc->add($data,'blog');
		redirect(base_url('Admin/addpost/'));
	}


	/// FOR IMAGE FUNCTION
	// print_r($this->upload->display_errors());
	//in case u need this, deactive return first!

private function _do_upload(){
		$config['upload_path'] = './images/_advisors';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	}

	private function _blog_upload(){
		$config['upload_path'] = './images/_blog';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']  = '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] 	= round(microtime(true)*1000);
		$config['encrypt_name'] = TRUE; 
		
		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload('photo'))
        {
            return "default.png";
        }
		else{
			return $this->upload->data('file_name');
		}
	}


}/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */