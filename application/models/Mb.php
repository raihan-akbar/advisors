<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mb extends CI_Model {

	function getAllBlog(){
		return $this->db->query('SELECT * FROM blog ORDER BY id_blog DESC');
	}

	function topThree(){
		return $this->db->query('SELECT * FROM blog ORDER BY id_blog DESC LIMIT 3');
	}

}

/* End of file Mb.php */
/* Location: ./application/models/Mb.php */