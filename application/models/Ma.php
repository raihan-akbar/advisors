<?php
class Ma extends CI_Model
{
	
	function getStudent(){
		return $this->db->query("SELECT * FROM student,expertise WHERE student.id_expertise=expertise.id_expertise");
		
	}

	function getCatchStudent(){
		$id_student = $this->session->userdata('id_student');
		return $this->db->query("SELECT * FROM student,expertise,student_token WHERE student.id_student='$id_student' AND expertise.id_expertise=student.id_expertise AND student_token.id_student=student.id_student ");
		
	}

	function getExpertise(){
		return $this->db->query("SELECT * FROM expertise ORDER BY name_expertise ASC");
		
	}

	function getOffense(){
		return $this->db->query("SELECT * FROM offense ORDER BY level_offense");
		
	}

	function studentWarnedList(){
		return $this->db->query("SELECT * FROM student WHERE student_stages LIKE 'Warned%'");
	}

	// CS : CatchStudent
	function getCSAchievement(){
		$id_student = $this->session->userdata('id_student');
		return $this->db->query("SELECT * FROM achievement WHERE id_student='$id_student' ");
	}

	function getCSWarningLetter(){
		$id_student = $this->session->userdata('id_student');
		return $this->db->query("SELECT * FROM warnings,user,student WHERE warnings.id_student=student.id_student AND warnings.id_user=user.id_user AND warnings.id_student='$id_student' ORDER BY id_warnings DESC");
	}

	function getCatchLetter(){
		$id_warnings = $this->session->userdata('id_warnings');
		return $this->db->query("SELECT * FROM warnings,user,student,expertise WHERE warnings.id_student=student.id_student AND warnings.id_user=user.id_user AND student.id_expertise = expertise.id_expertise AND warnings.id_warnings = '$id_warnings'");
	}

	function getAllLetter(){
		return $this->db->query("SELECT * FROM warnings,user,student,expertise WHERE warnings.id_student=student.id_student AND warnings.id_user=user.id_user AND student.id_expertise=expertise.id_expertise ORDER BY warnings.id_warnings DESC");
	}

	function getAllTeacher(){
		return $this->db->query("SELECT * FROM user WHERE id_role='2' ");
	}

	function getAllHomeroom(){
		return $this->db->query("SELECT * FROM user,homeroom,expertise WHERE user.id_user=homeroom.id_user AND homeroom.id_expertise=expertise.id_expertise ORDER BY name_expertise,grade");
	}

	function unameChecker($username,$id_user){
		return $this->db->query("SELECT * FROM user WHERE username='$username' AND id_user!='$id_user' ");
	}

	function mailChecker($email,$id_user){
		return $this->db->query("SELECT * FROM user WHERE email='$email' AND id_user!='$id_user' ");
	}

	function homeroomAvail($id_expertise,$grade){
		return $this->db->query("SELECT * FROM homeroom WHERE id_expertise='$id_expertise' AND grade='$grade' ");
	}


}//End