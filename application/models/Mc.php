<?php
class Mc extends CI_Model
{
	
	function get($table,$where){
		return $this->db->get_where($table,$where);
	}
	function add($data,$table){
		$this->db->insert($table,$data);
	}
	function del($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function upd($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function getMenu(){
		$role = $this->session->userdata('role');
		return $this->db->query("SELECT * FROM sidebar WHERE access='$role' ORDER BY sequence");

	}

	function userInfo(){
		$id_user = $this->session->userdata('id_user');
		return $this->db->query("SELECT * FROM user WHERE id_user='$id_user' ");
		
	}

	function detailUser(){
		$id_user = $this->session->userdata('id_user');
		return $this->db->query("SELECT * FROM user,role WHERE user.id_role=role.id_role AND user.id_user='$id_user' ");
			
	}

	function getUserList(){
		$id_user = $this->session->userdata('id_user');
		return $this->db->query("SELECT * FROM user,role WHERE user.id_role=role.id_role AND id_user!='$id_user' ");
	}

	function getRoleList(){
		return $this->db->query("SELECT * FROM role ");
	}

	function getRecordList(){
		return $this->db->query("SELECT * FROM student,record,offense,expertise,user WHERE student.id_student=record.id_student AND offense.id_offense=record.id_offense AND student.id_expertise=expertise.id_expertise AND record.id_user=user.id_user ORDER BY date_input DESC");
	}

	function notificationList(){
		return $this->db->query("SELECT * FROM student,record,offense,expertise,user WHERE student.id_student=record.id_student AND offense.id_offense=record.id_offense AND student.id_expertise=expertise.id_expertise AND record.id_user=user.id_user ORDER BY id_record DESC LIMIT 4");
	}

	function getAllReport(){
		return $this->db->query("SELECT * FROM report_violation,student,user,expertise,offense WHERE report_violation.id_reporter= user.id_user AND report_violation.id_student=student.id_student AND report_violation.id_offense=offense.id_offense AND expertise.id_expertise=student.id_expertise");
	}

	function getWaitReport(){
		return $this->db->query("SELECT * FROM report_violation,student,user,expertise,offense WHERE report_violation.id_reporter= user.id_user AND report_violation.id_student=student.id_student AND report_violation.id_offense=offense.id_offense AND expertise.id_expertise=student.id_expertise AND report_violation.stat='Pending' ");
	}

	function getAccReport(){
		return $this->db->query("SELECT * FROM report_violation,student,user,expertise,offense WHERE report_violation.id_reporter= user.id_user AND report_violation.id_student=student.id_student AND report_violation.id_offense=offense.id_offense AND expertise.id_expertise=student.id_expertise AND report_violation.stat='Accepted' ");
	}

	function getDecReport(){
		return $this->db->query("SELECT * FROM report_violation,student,user,expertise,offense WHERE report_violation.id_reporter= user.id_user AND report_violation.id_student=student.id_student AND report_violation.id_offense=offense.id_offense AND expertise.id_expertise=student.id_expertise AND report_violation.stat='Rejected' ");
	}

	function allPointLimit(){
		return $this->db->query("SELECT * FROM extras WHERE att='Point Limit' ");
	}

	function recSupervised(){
		$sv = $this->db->query("SELECT * FROM extras WHERE extras_name ='Supervised' ")->result();
		foreach ($sv as $gsv) {}
			$limit = $gsv->val_num;
		return $this->db->query("SELECT * FROM student WHERE point <='$limit' AND student_stages!='Warned 1' AND  student_stages!='Warned 2' AND student_stages!='Warned 3' ");
	}

	function recWarned1(){
		$sv = $this->db->query("SELECT * FROM extras WHERE extras_name ='Warned 1' ")->result();
		foreach ($sv as $gsv) {}
			$limit = $gsv->val_num;
		return $this->db->query("SELECT * FROM student WHERE point <='$limit' AND student_stages!='Warned 2' AND student_stages!='Warned 3' ");
	}

	function recWarned2(){
		$sv = $this->db->query("SELECT * FROM extras WHERE extras_name ='Warned 2' ")->result();
		foreach ($sv as $gsv) {}
			$limit = $gsv->val_num;
		return $this->db->query("SELECT * FROM student WHERE point <='$limit' AND student_stages!='Warned 1' AND  student_stages!='Warned 2'");
	}

	function recWarned3(){
		$sv = $this->db->query("SELECT * FROM extras WHERE extras_name ='Warned 2' ")->result();
		foreach ($sv as $gsv) {}
			$limit = $gsv->val_num;
		return $this->db->query("SELECT * FROM student WHERE point <='$limit' AND  student_stages!='Warned 3' ");
	}


}//End