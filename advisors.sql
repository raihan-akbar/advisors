-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2020 at 09:33 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advisors`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievement`
--

CREATE TABLE `achievement` (
  `id_achievement` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `achievement_title` varchar(50) NOT NULL,
  `achievement_date` date NOT NULL,
  `about_achievement` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `achievement`
--

INSERT INTO `achievement` (`id_achievement`, `id_student`, `id_user`, `achievement_title`, `achievement_date`, `about_achievement`) VALUES
(1, 4, 1, 'Best Student 2018', '0000-00-00', 'Fahaz Fardani is Best Student 2018 Season.'),
(2, 4, 1, ' 1st Place in Coloring Contest', '0000-00-00', 'Fahadz Fardani Memenangkan Juara 1 Lomba Mewarnai di TK Pasim'),
(3, 4, 1, 'Title', '2020-01-18', 'Explain This'),
(4, 1, 1, 'LKS Winner 3rd Speaking War', '2020-01-18', 'BLa BLa BLa Mother Fucker');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id_blog` int(11) NOT NULL,
  `blog_title` varchar(50) NOT NULL,
  `blog_content` text NOT NULL,
  `blog_type` varchar(50) NOT NULL,
  `blog_date` date NOT NULL,
  `blog_author` varchar(255) NOT NULL,
  `blog_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expertise`
--

CREATE TABLE `expertise` (
  `id_expertise` int(11) NOT NULL,
  `name_expertise` varchar(255) NOT NULL,
  `alias_expertise` varchar(50) NOT NULL,
  `about_expertise` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expertise`
--

INSERT INTO `expertise` (`id_expertise`, `name_expertise`, `alias_expertise`, `about_expertise`) VALUES
(1, 'Accounting', 'AKT', 'Accounting is the process of recording financial transactions pertaining to a business. The accounting process includes summarizing, analyzing, and reporting these transactions to oversight agencies, regulators, and tax collection entities.'),
(2, 'Animation', 'ANM', 'Animation is a method in which pictures are manipulated to appear as moving images. In traditional animation, images are drawn or painted by hand on transparent celluloid sheets to be photographed and exhibited on film. Today, most animations are made with computer-generated imagery (CGI).'),
(3, 'Software Engineering', 'RPL', 'A software engineer is a person who applies the principles of software engineering to the design, development, maintenance, testing, and evaluation of computer software.'),
(4, 'Broadcasting', 'TP4', 'broadcaster is someone who gives talks or takes part in interviews and discussions on radio or television programmes.'),
(5, 'Network Engineering', 'TKJ', 'network engineer is a technology professional who has the necessary skills to plan, implement and oversee the computer networks that support in-house voice, data, video and wireless network services.'),
(6, 'Office Administration', 'AP', 'Office administration is a set of day-to-day activities that are related to financial planning, record keeping & billing, personnel, physical distribution and logistics, within an organization.');

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `id_extras` int(11) NOT NULL,
  `extras_name` varchar(50) NOT NULL,
  `val_char` varchar(255) NOT NULL,
  `val_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`id_extras`, `extras_name`, `val_char`, `val_num`) VALUES
(1, 'supervised', '', -30),
(2, 'warned1', '', -45),
(3, 'warned2', '', -50),
(4, 'warned3', '', -55);

-- --------------------------------------------------------

--
-- Table structure for table `homeroom`
--

CREATE TABLE `homeroom` (
  `id_homeroom` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_expertise` int(11) NOT NULL,
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeroom`
--

INSERT INTO `homeroom` (`id_homeroom`, `id_user`, `id_expertise`, `grade`) VALUES
(2, 14, 3, 11),
(3, 13, 3, 12),
(4, 2, 2, 12),
(5, 16, 4, 12),
(6, 11, 4, 11),
(7, 15, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `offense`
--

CREATE TABLE `offense` (
  `id_offense` int(11) NOT NULL,
  `name_offense` varchar(255) NOT NULL,
  `point_offense` int(11) NOT NULL,
  `about_offense` text NOT NULL,
  `level_offense` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offense`
--

INSERT INTO `offense` (`id_offense`, `name_offense`, `point_offense`, `about_offense`, `level_offense`) VALUES
(1, 'Bullying', 0, 'Melakukan tindakan bullying kepada siswa/i', 'Mid'),
(2, 'Harassment', 0, 'Melakukan pelecehan', 'Mid'),
(3, 'Violence', 0, 'Melakukan tindakan kekerasan', 'High'),
(4, 'Smoking', 0, 'Merokok', 'Mid'),
(5, 'Vandalism', 0, 'Merusak Properti', 'Mid'),
(6, 'Test', 0, 'Test', 'Low');

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `id_record` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_offense` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `about_record` text NOT NULL,
  `date_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cost` int(11) NOT NULL,
  `d_record` date NOT NULL,
  `doc_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`id_record`, `id_student`, `id_offense`, `id_user`, `about_record`, `date_input`, `cost`, `d_record`, `doc_img`) VALUES
(1, 1, 1, 1, 'Melakukan Bullying', '2020-01-15 18:05:45', 4, '2020-01-16', '67e40231edb7722adb28d4f7eac8f3b0.png'),
(2, 1, 5, 1, 'Merusak Properti Milik Sekolah', '2020-01-15 18:06:16', 3, '2020-01-16', 'a73a15bb468d57ce8591a60a8527cd75.png'),
(3, 1, 6, 1, 'Test', '2020-01-27 07:03:14', 7, '2020-01-27', 'default.png'),
(4, 5, 1, 1, 'Dia Melakukan Tindakan bullying', '2020-02-03 13:53:24', 5, '2020-02-03', 'default.png'),
(5, 12, 5, 1, 'Merusak Propeti Milik Sekolah Dengan Sengaja', '2020-02-04 02:03:58', 2, '2020-02-04', '86fed5dbfd861f355dc76d43fe8b40f8.png'),
(6, 2, 1, 17, 'Dikenakan Sanksi pengurangan point', '2020-02-04 03:40:15', 3, '2020-02-04', 'default.png');

--
-- Triggers `record`
--
DELIMITER $$
CREATE TRIGGER `Reduce Point by Cost` AFTER INSERT ON `record` FOR EACH ROW BEGIN
UPDATE student SET point=point-NEW.cost
WHERE id_student=NEW.id_student;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `name_role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `name_role`) VALUES
(1, 'Advisors'),
(2, 'Teacher'),
(3, 'Student'),
(4, 'Admin'),
(5, 'Homeroom');

-- --------------------------------------------------------

--
-- Table structure for table `sidebar`
--

CREATE TABLE `sidebar` (
  `id_sidebar` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `sequence` int(11) NOT NULL,
  `access` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sidebar`
--

INSERT INTO `sidebar` (`id_sidebar`, `title`, `icon`, `url`, `sequence`, `access`) VALUES
(1, 'Dashboard', 'fa fa-map', 'Master/', 1, 'Advisors'),
(3, 'Student', 'fa fa-users', 'Master/students/', 3, 'Advisors'),
(4, 'Offense', 'fa fa-flag', 'Master/offense/', 5, 'Advisors'),
(5, 'Expertise', 'fa fa-code-fork', 'Master/expertise/', 4, 'Advisors'),
(6, 'Accounts', 'fa fa-user', 'Master/account/', 2, 'Advisors'),
(9, 'Dashboard', 'fa fa-map', 'Admin/', 1, 'Admin'),
(10, 'Users', 'fa fa-users', 'Admin/users/', 2, 'Admin'),
(11, 'Account', 'fa fa-user', 'Admin/account/', 3, 'Admin'),
(12, 'Record', 'fa fa-book', 'Master/records/', 3, 'Advisors'),
(13, 'Point Limit', 'fa fa-connectdevelop', 'Admin/point_limit/', 6, 'Admin'),
(14, 'Letter', 'fa fa-inbox', 'Master/letter', 5, 'Advisors'),
(15, 'Dashboard', 'fa fa-map', 'Homeroom/', 1, 'Homeroom'),
(16, 'Student', 'fa fa-users', 'Homeroom/student', 2, 'Homeroom'),
(17, 'Homeroom', 'fa fa-user-secret', 'Admin/homeroom', 3, 'Admin'),
(18, 'Dashboard', 'fa fa-map', 'Student/', 1, 'Student'),
(19, 'Profile', 'fa fa-user', 'Student/profile', 2, 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id_student` int(11) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `id_expertise` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `start_period` year(4) NOT NULL,
  `status` varchar(20) NOT NULL,
  `grade` int(11) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `student_stages` varchar(25) NOT NULL,
  `student_avatar` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `student_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id_student`, `nis`, `name`, `address`, `id_expertise`, `point`, `start_period`, `status`, `grade`, `gender`, `student_stages`, `student_avatar`, `password`, `student_email`) VALUES
(1, '171800136', 'Arethusa Maulana Kawakibi Hidayat', 'Indonesia\r\nSukabumi, West Java\r\nBaros (Genting Puri Housing)\r\nRt. 01 Rw.023', 3, -14, 2017, 'Active', 12, 'Male', 'Warned 2', 'ec8c35fb165c2c1651552778279ca270.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(2, '171800137', 'Fabio Syechan Pamungkas', 'Indonesia\r\nWest Java, Sukabumi\r\nSalbin Rt.0 Rw.0', 3, -3, 2017, 'Active', 12, 'Male', 'Warned 3', '7d86c29dd2e799d9e946324c13713369.png', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(3, '171800138', 'Fadel Ramadhan Iskandar', 'Indonesia\r\nWest Java, Sukabumi\r\nJalur Sukaraja Rt. 0 Rw. 0', 3, 0, 2017, 'Active', 11, 'Male', 'Warned 1', 'default.png', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(4, '171800140', 'Fahadz Fardani', 'Indonesia\r\nWest Java, Sukabumi', 3, 0, 2017, 'Active', 12, 'Male', 'Supervised', '00fa6bbe452a6b96ce0bea5fb3c39445.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(5, '171800113', 'Mochammad Rifki Ariq Hanyakal', 'Rawan Kecelakaan', 3, -5, 2019, 'Active', 12, 'Male', 'Warned 1', 'b26f91225470c38bc06ffcc315242626.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(6, '171800148', 'Muhammad Raihan Arsyi Faikal', 'Indonesia\r\nWest Java, Sukabumi (43231)', 2, 0, 2017, 'Active', 12, 'Male', 'Ordinary', '0e43b77e4a416a793f909b2fd97967d7.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(7, '171800155', 'Muchammad Azis', 'Indonesia\r\nWest Java, Sukabumi', 3, 0, 2017, 'Active', 12, 'Male', 'Warned 2', '54dac09dcfe4cc90513edb5fc1b1c6f7.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(8, '171800268', 'Muhammad Cece Rami', 'Indonesia\r\nWest Java, Sukabumi', 3, 0, 2017, 'Active', 12, 'Male', 'Warned 3', 'c7b10b9422b0a837888fcf01373733a1.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(9, '171800130', 'Satria Diaz Ilayaza', 'Indonesia\r\nWest Java, Sukabumi', 3, 0, 2017, 'Active', 12, 'Male', 'Ordinary', '181c994a05033df26e68dc0b86c6c99d.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(10, '171800150', 'Mala Mulyahati', 'Cicadas ', 3, 0, 2017, 'Active', 12, 'Female', 'Warned 2', 'f0eb5e0fe99b7cb5cbb55c0d7ef66d43.png', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(11, '171810223', 'Reza Jamet', 'PCP', 1, 0, 2017, 'Active', 12, 'Male', 'Warned 1', '6868095db3ec5880e650416d33d003de.jpg', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(12, '171800325', 'Hadi Wiryanto', 'Jln. Subangjaya', 2, -2, 2017, 'Active', 12, 'Male', 'Warned 1', 'default.png', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(13, '12345', '1234', '12345', 2, 0, 2020, 'Active', 12, 'Male', 'Ordinary', '1c72c152bd4dc398d7c269b2b50ed010.png', '$2y$12$/SbRCLZwngV92JgJqrgOA.pXHhQkw/9O0b7z81XSvCqTxDBHtAA7e', ''),
(18, '09', '09', '09', 5, 0, 2020, 'Active', 11, 'Male', 'Ordinary', 'default.png', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_token`
--

CREATE TABLE `student_token` (
  `id_student_token` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `temp` varchar(255) NOT NULL,
  `exp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_token`
--

INSERT INTO `student_token` (`id_student_token`, `id_student`, `token`, `temp`, `exp`) VALUES
(2, 18, '18bnzmozecz', 'exit', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `s_name` varchar(255) NOT NULL,
  `id_role` int(11) NOT NULL,
  `user_avatar` varchar(255) NOT NULL,
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `s_name`, `id_role`, `user_avatar`, `since`, `email`) VALUES
(1, 'advisors', '$2y$12$D.0PUz.OsQcihYZAUMKZuuxxbWgum9g29l1fB7iicVLEwWaoaEatO', 'Frank Lampard', 1, '03ca7ff11585407fe545b5e925b9a560.png', '2019-10-08 22:03:15', 'raihangl1@gmail.com'),
(2, 'teacher', '$2y$12$RYsE24SSXNZo325s6jwaEeUs94tNeRk.a0s3Q13pAuVeitvuMPUEm', 'Ernesto Valverde', 5, 'default.png', '0000-00-00 00:00:00', 'raihan.akbar@outlook.com'),
(6, 'felkies', '$2y$12$mylW1IjBn8KK2pDvcit7zeATIOFu9hl9wahXI3qbhp33r2lsB3adC', 'Felkies', 1, 'default.png', '2019-10-07 15:11:29', 'hello@felkies.dx.am'),
(7, 'master', '$2y$12$cH.8EEeE6D/.g8MGhGR0GOFpofEtQT3FmA3sNmc4hx3D.HlZTjmJS', 'Master Test', 1, 'default.png', '2019-12-04 06:49:20', 'master@master.com'),
(10, 'admin', '$2y$12$a1jvMnrK8sUKgeYS4scruuAzRSOdMjXdux4o0SQaskmoClLRq9Iyy', 'Admin', 4, '2fb7a37711e79b733f4fe315f57c8049.png', '2020-01-13 07:51:33', 'raihangl19@gmail.com'),
(11, 'tester', '$2y$12$Cfl6OUnRgmSMHSOF6ny6feTiQbbUX7ORBjwjdf9QJ/..SXefSyQ/S', 'Tester', 5, 'default.png', '2020-01-30 09:24:55', 'bluesht26@gmail.com'),
(12, 'wadaw', '$2y$12$E73jAERVLKQOWj5W5Dqm1eZdKd3inR5/pMEwncGgNG5N2l7XJpI8e', 'Wadaw', 1, 'default.png', '2020-01-30 17:12:47', 'timberwolf496@gmail.com'),
(13, 'iki', '$2y$12$zWlSATuRDnjhSTtWlJiXW.lCSsXYoFcu1dszMMn7ax12dxaKvZdhu', 'iki', 5, 'default.png', '2020-01-31 00:24:53', 'shield.broth3r@gmail.com'),
(14, 'thusa', '$2y$12$RflOL2iRLBaBbvFjhbNxXOtM94r72yxZ2a1yshAr/uC7DdyXGL/2a', 'Thusa', 5, 'default.png', '2020-01-31 00:37:56', 'askthusa@gmail.com'),
(15, 'bad', '$2y$12$FzRYWINPlHTEZRVAYhukrOGeXzHHnYcr1tW840psFSF1WK3qLP/qW', 'bad', 5, 'default.png', '2020-01-31 18:01:35', 'raihan.gl@protonmail.com'),
(16, 'jamet', '$2y$12$ycbOAWcZBArozVWWhUceOOKrGfYLeJqjnxGCprL68gCAwb3GkXCx2', 'Jamet', 5, 'cce439603acdfc1f6a5dd8b70b87eabd.jpg', '2020-02-02 10:33:55', 'jamet@jamet.com'),
(17, 'raihanakbar', '$2y$12$830bNMmZPwORR9XHu.hhtemPZaJTdd/nx1qyfLQrVwylUSj4hBdUy', 'Raihan Akbar', 2, '0dbbd288e3d4924850ed9b9c8bcaaf8c.png', '2020-02-04 03:37:56', 'rhnmhmd19@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id_user_token` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `temp` varchar(255) NOT NULL,
  `exp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id_user_token`, `id_user`, `token`, `temp`, `exp`) VALUES
(3, 10, '20AbfFz', 'jabbuffenjhfxioi', '2020-02-06'),
(4, 11, '11sklaz', 'vgi5grolpcldwfvv', '2020-02-06'),
(5, 12, '12eihsz', 'ful6ocvponsdnwpk', '0000-00-00'),
(6, 13, '13qb7bz', 'exit', '0000-00-00'),
(7, 14, '14dklsz', 'slwtg8qigjpityrs', '0000-00-00'),
(8, 15, '15quyez', 'otuktc9s', '0000-00-00'),
(9, 16, '16l6wdpqz', '3rwqxbwlwlgnqygh', '2020-02-04'),
(10, 17, '17hm9ccoz', 'exit', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `id_warnings` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `warned_type` varchar(50) NOT NULL,
  `letter_title` varchar(150) NOT NULL,
  `concerning_subject` varchar(50) NOT NULL,
  `date_show` varchar(50) NOT NULL,
  `letter_reason` text NOT NULL,
  `letter_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warnings`
--

INSERT INTO `warnings` (`id_warnings`, `id_user`, `id_student`, `warned_type`, `letter_title`, `concerning_subject`, `date_show`, `letter_reason`, `letter_input`) VALUES
(1, 1, 1, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '18September2020', 'Karena jumlah Point yang siswa peroleh melebihi batas point yang telah di setujui dan di sepakati melalui perundingan guru-guru dan kesiswaan.', '0000-00-00 00:00:00'),
(2, 1, 1, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '20, Maret 2020', 'Karena Murid nya emang gitu diamah kaya yang pengen tampol murid nya teh.', '0000-00-00 00:00:00'),
(3, 1, 2, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '10, Januari 2020', 'Karena Fabio Seorang Dedemyth.', '0000-00-00 00:00:00'),
(4, 1, 4, 'Warned 1', 'SURAT PERINGATAN', 'Surat Peringatan 1', '13, Maret 2020', 'Fahaz Fardani Telah Melukan Tindakan Pelecehan terhadap Arethusa Maulana(Teman Sekelasnya).', '0000-00-00 00:00:00'),
(5, 1, 4, 'Warned 2', 'SURAT PERINGATAN', 'Surat Peringatan 2', '19, Maret 2020', 'Fahaz Telah Melanggar perjanjian yang telah dibuat dan disetujui didalam Surat Peringatan 1', '0000-00-00 00:00:00'),
(6, 1, 4, 'Warned 3', 'Surat Peringatan', 'Surat Peringatan 3', '4, Februari 2020', 'Fahaz Telah Melanggar Aturan dan Tata Tertib Sekolah dan perkaluan nya tidak bisa di toleran oleh pihak sekolah, Kami Telah Memutuskan \r\nuntuk <b>Mengeluarkan</b> Fahaz dari Sekolah.', '0000-00-00 00:00:00'),
(7, 1, 4, 'Warned 2', 'Waw', 'Waw', '21, Februari 2020', 'Dikarenakan Fahaz Menyelamatankan Bumi,\r\nKami telah berdiskusi panjang dan telah menyetujui bahwa surat kemarin yang kamu di keluarin itu, Ga jadi kami kasih ke kamu.\r\nSoalnya Kamu menyelamatkan Bumi\r\n-Terimakasih Fahaz :)', '0000-00-00 00:00:00'),
(8, 1, 2, 'Warned 2', 'Surat Peringatan', 'Surat Peringatan 2', '9, Juni 2020', 'Karena Fabio Ayam dia kena SP2', '0000-00-00 00:00:00'),
(9, 1, 2, 'Warned 3', 'Fabio Ayam', 'Surat Ayam', '3, Februari 2020', 'Fabio Ayam', '0000-00-00 00:00:00'),
(10, 1, 2, 'Warned 2', '22222', '1232', '1, Januari 2020', '111', '0000-00-00 00:00:00'),
(11, 1, 2, 'Warned 2', 'asas', 'sladk', '7, Mei 2020', 'a', '0000-00-00 00:00:00'),
(12, 1, 2, 'Warned 2', 'SURAT PERINGATAN', 'Surat Peringatan 2', '19, Juni 2020', 'Karena Siswa/i telah melakukan banyak pelanggaran terhadap Peraturan dan Tata Tertib Sekolah, Oleh karena itu Poin yang dimiliki melebihi batas yang telah kami tentukan.', '0000-00-00 00:00:00'),
(13, 1, 2, 'Warned 3', 'MCR', 'MCR', '1, Maret 2020', 'MCR MCR', '0000-00-00 00:00:00'),
(14, 1, 10, 'Warned 1', 'Test', 'Test Aja', '2, Februari 2020', 'Test Bro', '0000-00-00 00:00:00'),
(15, 1, 10, 'Warned 2', 'B', 'B', '19, Februari 2020', 'AAA', '0000-00-00 00:00:00'),
(16, 1, 2, 'Warned 3', 'Surat Peringatan', 'Surat Peringatan 3', '4, Maret 2020', 'Karena mau nyoba aja', '0000-00-00 00:00:00'),
(17, 1, 3, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '18, April 2020', 'Karena Poin Siswa Tersebut Melebihi Batas.', '0000-00-00 00:00:00'),
(18, 1, 8, 'Warned 1', 'SURAT PERINGATAN', 'Surat Peringatan 1', '13, Juni 2020', 'Test 1', '2020-01-27 07:00:49'),
(19, 1, 11, 'Warned 1', 'SURAT PERINGATAN', 'Surat Peringatan 1', '2, Februari 2020', 'Karena Poin Siswa/i tersebut melebihi batas yang di tentukan.', '2020-02-02 10:21:01'),
(20, 1, 1, 'Warned 2', 'Surat Peringatan', 'Surat Peringatan 2', '04, Februari 2020', 'Karena Foto dia Jele Sekalee', '2020-02-03 17:22:10'),
(21, 1, 12, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '4, Februari 2020', 'Karena Siswa Melakukan tindakan yang sangat tidak di inginkan, dan mempengaruhi siswa lain untuk melanggar pelanggaran.', '2020-02-04 02:06:00'),
(22, 17, 5, 'Warned 1', 'Surat Peringatan', 'Surat Peringatan 1', '4, Februari 2020', 'Karena Poin Siswa Melebihi batas yang ditemtukan oleh pihak sekolah.', '2020-02-04 03:45:06'),
(23, 1, 7, 'Warned 1', 'Test Email', 'TEst Email', '2, Februari 2020', 'Homeroom Auto Alert Email', '2020-02-10 17:02:40'),
(24, 1, 7, 'Warned 2', 'Test 2', 'Test Email 2', '2, Maret 2020', 'Cihuy test 1 One Shot wkwkwk', '2020-02-10 17:08:12'),
(25, 1, 8, 'Warned 2', 'Test Email 3', 'Test Email', '1, Januari 2020', 'Cece! Your Score is Dark u know?', '2020-02-10 17:13:22'),
(26, 1, 8, 'Warned 3', 'SP3', 'SP', '1, Januari 2020', 'Dikarenakan murid tersebut telah melakukan banyak pelanggaran yang sebelumnya telah di sepakati agar tidak di langgar oleh sekolah, Detail nya seperti standing motor di sekolah, salto saat jam pelajaran dsb. Oleh karena itu kami dari pihak kesiswaan memberikan Surat Peringatan 3 Kepada murid tersebut \r\ntqtq.', '2020-02-10 17:18:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievement`
--
ALTER TABLE `achievement`
  ADD PRIMARY KEY (`id_achievement`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `expertise`
--
ALTER TABLE `expertise`
  ADD PRIMARY KEY (`id_expertise`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id_extras`);

--
-- Indexes for table `homeroom`
--
ALTER TABLE `homeroom`
  ADD PRIMARY KEY (`id_homeroom`);

--
-- Indexes for table `offense`
--
ALTER TABLE `offense`
  ADD PRIMARY KEY (`id_offense`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id_record`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `sidebar`
--
ALTER TABLE `sidebar`
  ADD PRIMARY KEY (`id_sidebar`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_student`);

--
-- Indexes for table `student_token`
--
ALTER TABLE `student_token`
  ADD PRIMARY KEY (`id_student_token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id_user_token`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`id_warnings`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievement`
--
ALTER TABLE `achievement`
  MODIFY `id_achievement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expertise`
--
ALTER TABLE `expertise`
  MODIFY `id_expertise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `id_extras` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `homeroom`
--
ALTER TABLE `homeroom`
  MODIFY `id_homeroom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `offense`
--
ALTER TABLE `offense`
  MODIFY `id_offense` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `id_record` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sidebar`
--
ALTER TABLE `sidebar`
  MODIFY `id_sidebar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `student_token`
--
ALTER TABLE `student_token`
  MODIFY `id_student_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id_user_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `id_warnings` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
